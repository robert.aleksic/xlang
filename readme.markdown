Introduction
============

Hex is small and simple micropocessor designed by prof. David May at Bristol university for teaching computer science. Memory is treated either as array of words (for data access) or bytes (for instruction access). For hex8, word is 1 byte, hex16 2 bytes etc. It have 2 registers, `areg` and `breg`. It's instructions are one byte, regardles of microprocesor word length. Instructions might have one word operand, stored in `oreg`. If operand is bigger then 4 bits, it can be extended with `pfix` and `nfix` instructions. On startup, program counter `pc` is set to zero, and instructions are fetched byte by byte. Data addresses are aligned on processor word length.

X is higer level langyage with compiler in X.

This is small toolset writen in free pascal to support playing with hex processor and x language.

It contains:

  * hex   - interpreter for all byte widths (hex8,16,32 and 64)
  * xpas  - pascal compiler x -> hex code (rewriten original compiler)
  * x     - x compiler in x
  * x.bin - compiler in hex code (produced by xpas or x for bootstraping)

  * hex sim - some experiments with hex assembler

  * sublime text syntax highlighting and build system


Instalation
-----------

Install freepascal, copy sublime folder to Packages/User for sublime support.

To compile xhex.x with xhex.pas producing xhex.bin
To self compile compiler run xhex.bin in hex with x.x as input file, it will produce f2.out code identical to xhex.bin


Further ideas
-------------
X language:
  - currently compiler is quite fragile, should be more robust
  - compiler stop on first error in x also
  - some sort of code analisys and normalisation

  - forward declarations for ll1 compiling
  - procedures and function called only once inlined with parameter substitution, beware of recursion
  - proc/func nesting (at least for inlines) to improve code structuring
  - parameter type checking
  - standard put and get, putw/getw?, in/out?, syscall
  - wordsize designation, compiler probbably only for >16, but cross compiling possible
  - strengthen compiler, first in pascal, then in x
  - coma separated const, var and arrays in declarations
  - enum constants

  - byte type?
  - local arrays?
  - var parameters?
  - no () for no params
  - records?
  - structured types
  - byte packing?, dynamic arrays/strings
  - inline assembly without prefixes
  - for loops or some sort of loops
  - maybe dijkstras do/od and if/fi
  - true and false constants
  - multiple assigment
  - aliases like in occam?
  - absolute positioning, eventual retyping

Hex:
  - code file compatibility - first byte wordsize, next words - const, var and array sizes, codesize, code bytes, word checksmum? (with careful design some code might run even on lower bytesizes resize)
  - br 0, br -1 as stop? (old pc - new pc -> stop?)
  - undefined behaviour as stop?
  -
  - reorder instructions brb instead of stop with branch on register b
  - introduce workspace pointer?
  - mem[0] - wordsize, mem[1] - sp, mem[2] - memorysize in words

Interpreter compiler for some processors (arm, z80?)
