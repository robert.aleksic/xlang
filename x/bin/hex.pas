program hex;

var inum, pnum, brnum, s1num, s2num, i64 : int64;

const wordsize = 4;        //     1,      2,          4,       8
type  word     = longint;  // shortint, smallint, longint,   int64 or
                           // byte,     word,     longword,  qword  integers

const memsize  = 200000;   // in words

      nfixmask = word (-1) shl 8;
      mostneg  = word ( 1) shl (wordsize*8-1);

      files    = 8;

type  instruction  = (ldam,ldbm,stam,    ldac,ldbc,ldap,  ldai,ldbi,stai,
                      br,brz,brn, stop,  opr,pfix,nfix,   brb,add,sub,svc);

const inststr      : array [instruction] of string =
                      ('ldam','ldbm','stam',      'ldac','ldbc','ldap',  'ldai','ldbi','stai',
                       'br','brz','brn', 'stop',  'opr','pfix','nfix',   'brb','add','sub','svc');

var   wmem : array [0..memsize-1]          of word;
      bmem : array [0..memsize*wordsize-1] of byte absolute wmem;

      f    : array [0..files-1] of file of byte;
      fin  : file of byte;
      open : array [0..files-1] of boolean;

      infile, binfile : string;



  procedure load (fn:string);
  var
    f : file of byte;
    i : word;
    b : byte;

    inum,pnum : integer;

  begin
    assign (f,fn);
    reset  (f);

    inum := 0;
    pnum := 0;

    for i := 1 to wordsize do read (f,b);

    i := 0;
    while not eof (f) do
    begin
      read (f, bmem [i]);
      case instruction (bmem[i] div 16) of
         pfix,nfix : pnum := pnum+1;
         else
      end;
      i := i+1
    end;
    inum := i;

    writeln ('total: ',inum,
             ' pfix,nfix: ',pnum,' (',pnum/inum*100:4:1,'%)');
    close (f)
  end;

  procedure doopen (i:integer; write:boolean);

    function fname : string;
    begin
      fname := 'f' + chr (ord('0')+i);
      if write
      then fname := fname + '.out'
      else fname := fname + '.in'
    end;

  begin
    if not open[i]
    then begin
           assign (f[i],fname);
           if write
           then rewrite (f[i])
           else reset  (f[i]);
           open [i] := true
         end
  end;



  // maybe to cleanup those numbers 0 - stdin/stdout, 1 - kbd/stderr, 2..files+1 - files

  // input  : stream 0 - infile, 1..255 - sysin, bits 8..10 files f0..7.in
  // output : stream 0..255 - sysout,            bits 8..10 files f0..7.out

  procedure put (s,b : word);
  begin
    if s shr 8 = 0
    then write (chr (b and $ff))
    else begin
           s := (s shr 8) mod files;
           doopen (s, true);
           write (f[s], b and $ff)
         end
  end;

  function get (s : word) : word;
  var b : byte;
  begin
    if s shr 8 = 0
    then if s <> 0
         then read (b)  // readkey on stream 1?
         else if eof(fin) or (infile='') then b := 255 else read (fin, b)
    else begin
           s := (s shr 8) mod files;
           doopen (s, false);
           read (f[s], b);
         end;
    get := b
  end;



var
  areg, breg, oreg, pc : word;
  running, clear       : boolean;

  sp   : word;        // at wmem [1]
  inst : instruction;

  b : byte;

  insnum : array [instruction] of int64;
  ind    : array [instruction] of instruction;
  inst2,inst3  : instruction;

begin
  for inst := low(instruction) to high(instruction) do
    insnum[inst] := 0;

  infile   := 'xhex.x';
  if paramstr (1)<>''
  then binfile := paramstr(1)+'.bin'
  else binfile := 'f2.out';

  if infile<>''
  then begin
         assign (fin,infile);
         reset  (fin);
       end;
  for b := 0 to files-1 do
    open [b] := false;

  load (binfile);

  inum := 0;
  pnum := 0;
  brnum := 0;
  s1num := 0;
  s2num := 0;

  oreg := 0;
  pc   := 0;

  running := true;
  while running do
  begin
    inum := inum+1;

    b    := bmem [pc];
    pc   := pc + 1;
    oreg := oreg + b and $f;
    inst := instruction (b shr 4);

    if inst=opr
    then case oreg + ord (brb) of
           ord (brb) : inst := brb;
           ord (add) : inst := add;
           ord (sub) : inst := sub;
           ord (svc) : inst := svc;
           else inst := stop
         end;

    insnum[inst] := insnum[inst]+1;
    {if (inst<>pfix) and (inst<>nfix)
    then begin
           write (pc-1,': ');
           sp := wmem[1];
           case inst of
             stop, add, sub : write (inststr[inst]);
             brb            : write (inststr[inst],' ',int32 (breg));
             svc : case areg of
                     0 : write ('stop');
                     1 : write ('put ',wmem [sp+2],', ',wmem [sp+3]);
                     2 : write ('get ',wmem [sp+2],', ',sp+1);
                     else write ('stop')
                   end;
             else write (inststr[inst],' ',int32(oreg));
          end;
          write ('; ')
        end;}

    clear := true;
    case inst of
      ldam : areg := wmem [oreg];
      ldbm : breg := wmem [oreg];
      stam : wmem [oreg] := areg;

      ldac : areg := oreg;
      ldbc : breg := oreg;
      ldap : areg := pc + oreg;

      ldai : areg := wmem [areg + oreg];
      ldbi : breg := wmem [breg + oreg];
      stai : wmem [breg + oreg] := areg;

      brn  : if not (areg and mostneg = 0) then begin pc := pc + oreg; brnum := brnum+1 end;
      brz  : if      areg             = 0  then begin pc := pc + oreg; brnum := brnum+1 end;
      br   :                                    begin pc := pc + oreg; brnum := brnum+1 end;

      pfix : begin oreg :=              oreg shl 4;  clear := false; pnum := pnum+1 end;
      nfix : begin oreg := nfixmask or (oreg shl 4); clear := false; pnum := pnum+1 end;

      brb  : begin pc := breg; brnum := brnum+1 end;
      add  : areg := areg + breg;
      sub  : areg := areg - breg;
      svc  : begin
               sp := wmem [1];
               case areg of
                 0 : running := false;
                 1 : begin put (wmem [sp+3], wmem [sp+2]); s1num := s1num+1 end;
                 2 : begin wmem [sp+1] := get (wmem [sp+2]) and 255; s2num := s2num+2 end;
                 else running := false
               end
             end;
       else running := false
    end;

    if clear then oreg := 0
  end;

  for b := 0 to files-1 do
    if open [b]
    then close (f[b]);
  if infile<>'' then close (fin);

  writeln ('inst: ',inum,
           ' pfix,nfix: ',pnum,' (',pnum/inum*100:4:1,'%)',
           ' put: ',s1num,' (',s1num/inum*100:4:1,'%)',
           ' get: ',s2num,' (',s2num/inum*100:4:1,'%)',
           ' brnum: ',brnum,' (',brnum/inum*100:4:1,'%)');

  for inst := low(instruction) to high(instruction) do
    ind[inst] := inst;

  for inst := low(instruction) to pred (high(instruction)) do
    for inst2 := succ (inst) to high(instruction) do
      if insnum[ind[inst]]<insnum[ind[inst2]]
      then begin
             inst3 := ind[inst2];
             ind[inst2] := ind[inst];
             ind[inst]  := inst3;
           end;

  for inst := low(instruction) to high(instruction) do
    writeln (inststr[ind[inst]]:6,': ', insnum[ind[inst]]:11,' (',insnum[ind[inst]]/inum*100:4:1,'%) ');

end.