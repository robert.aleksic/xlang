unit base;
interface

type  word         = int32; // for unsigned longword; use negx instead of < 0, comparison with lsu
const bytesperword = 4;
type  wstring      = array [0..100] of word;

var
  fname       : string;
  linecount   : word;

const instream      =   0;
      messagestream =   0;
      logstream     =   1;
      binstream     = 512;

      eof = 255;
      eol =  10;
      cr  =  13;
      tab =   9;

var   outstream : word;



  // math
  function mul  (n,m : word) : word; // n * m
  function divf (n,m : word) : word; // n div m
  function rem  (n,m : word) : word; // n mod m

  function mul2 (x,y : word) : word; // x * y, fast multiply when y = 2^n
  function exp2 (n   : word) : word; // 2 ^ n

  // strings
  function  packstring   (s : wstring; var v : wstring) : word;
  procedure unpackstring (s : wstring; var v : wstring);

  function  ws (s : string) : wstring; // to be as close to original x code as possible

  // io
  procedure init;
  procedure selectoutput (c : word);
  procedure done         (w : word);

  procedure putval (c : word);
  function  getchar : word;
  procedure newline;

  procedure prints   (s : wstring);
  procedure printn   (n : word);
  procedure printhex (n : word);

  procedure cmperror (s : wstring);

implementation
var
  div_x : word;

  // math

  function negx (w:word) : boolean; inline;
  begin
    negx := not ((w and word($80000000)) = 0)
  end;

  // signed less via unsigned less; x<0 is x and mostneg <> 0
  function lsu (x,y : word) : boolean;
  begin
    if (x<0) = (y<0)
    then lsu := x<y
    else lsu := y<0
  end;

  function mul (n,m : word) : word;
  var mul_x : word;

    function mul_step (b,y : word) : word;
    var r : word;
    begin
      if (b<0) or (not lsu (b, mul_x))
      then r := 0
      else r := mul_step (b + b, y + y);
      if not lsu (mul_x, b)
      then begin
             mul_x := mul_x - b;
             r := r + y
           end;
      mul_step := r
    end;

  begin
    mul_x := m;
    mul := mul_step (1, n)
  end;

  function divf (n,m : word) : word;

    function div_step (b,y : word) : word;
    var r : word;
    begin
      if (y<0) or (not lsu (y,div_x))
      then r := 0
      else r := div_step (b + b, y + y);
      if not lsu (div_x, y)
      then begin
             div_x := div_x - y;
             r := r + b
           end;
      div_step := r
    end;

  begin
    div_x := n;
    if lsu (n, m)
    then divf := 0
    else divf := div_step (1, m)
  end;

  function rem (n,m : word) : word;
  var x : word;
  begin
    x := divf (n,m);
    rem := div_x
  end;

  function mul2 (x,y : word) : word;
  var n, r : word;
  begin
    r := x; n := 1;
    while n <> y do
    begin
      r := r + r;
      n := n + n
    end;
    mul2 := r
  end;

  function exp2 (n : word) : word;
  var r, i : word;
  begin
    i := n;
    r := 1;
    while i > 0 do
    begin
      r := r + r;
      i := i - 1
    end;
    exp2 := r
  end;



  // strings

  function packstring (s : wstring; var v : wstring) : word;
  var n, si, vi, w, b : word;
  begin
    n := s[0];
    si := 0; vi := 0;
    w  := 0; b  := 0;
    while si <= n do
    begin
      w := w + mul (s[si], exp2 (mul2 (b, 8)));
      b := b + 1;
      if b = bytesperword
      then begin
             v[vi] := w;
             vi := vi + 1;
             w := 0; b := 0
           end;
      si := si + 1
    end;
    if b = 0
    then vi := vi - 1
    else v[vi] := w;
    packstring := vi
  end;

  procedure unpackstring (s : wstring; var v : wstring); // !!! string overflow
  var n, si, vi, w, b : word;
  begin
    si := 0; vi := 0; b := 0;
    w := s[0]; n := rem (w, 256);
    while vi <= n do
    begin
      v[vi] := rem  (w, 256);
      w     := divf (w, 256);

      vi := vi + 1;
      b  := b  + 1;
      if b = bytesperword
      then begin
             b  := 0;
             si := si + 1;
             w  := s[si]
           end
    end
  end;

  function ws (s:string) : wstring;
  var charv : wstring; charp : word;
  begin
    charp := 0;
    while charp < length (s) do
    begin
      charp := charp+1;
      charv [charp] := ord (s [charp])
    end;
    charv [0] := charp;
    charp := packstring (charv, ws)
  end;


var fin, fbin : text; // flog : text;

  procedure init;
  begin
    assign (fin, fname+'.x');   reset   (fin);
    assign (fbin,fname+'.bin'); rewrite (fbin);
    //assign (flog,fname+'.log'); rewrite (flog)
  end;

  procedure done (w : word);
  begin
    close (fin);
    close (fbin);
    //close (flog);
    halt (w)
  end;

  procedure putval (c : word);
  begin
    if outstream = messagestream
    then if c=eol
         then writeln
         else write (chr (c))
    else if c=eol
         then writeln (fbin)
         else write (fbin, chr (c))
  end;

  function getchar : word;
  var ch : char;
  begin
    if system.eof (fin)
    then getchar := eof
    else if eoln (fin)
         then begin
                readln (fin);
                getchar := eol
              end
         else begin
                read (fin, ch);
                getchar := ord(ch)
              end
  end;



  // io

  procedure selectoutput (c : word);
  begin
    outstream := c
  end;

  procedure newline;
  begin
    putval (eol)
  end;

  procedure prints (s : wstring);
  var n,p,w,l,b : word;
  begin
    p := 0; w := s[p];
    l := rem  (w, 256);

    w := divf (w, 256);
    n := 1; b := 1;
    while n <= l do
    begin
      putval (rem (w, 256));
      w := divf (w, 256);

      n := n + 1;
      b := b + 1;
      if b = bytesperword
      then begin
             b := 0;
             p := p + 1;
             w := s[p]
           end
    end
  end;

  procedure printn (n:word);
  begin
    if n<0
    then begin
           putval (ord('-'));
           printn (-n)
         end
    else begin
           if n>9
           then printn (divf(n,10));
           putval (rem (n,10) + ord('0'))
         end
  end;

  procedure printhex (n:word);
  var d : word;
  begin
    d := divf (n, 16);
    if d <> 0
    then printhex (d);

    d := rem (n, 16);
    if d < 10
    then putval (d + ord('0'))
    else putval ((d-10) + ord('a'))
  end;



  procedure cmperror (s : wstring);
  begin
    prints (ws('error near line '));
    printn (linecount);
    prints (ws(': '));
    prints (s);
    newline;
    done (1)
  end;

end.