unit lex;
interface
uses base;

const
  linemax       =    200;  // line buff
  nametablesize =    101;  // name hash table
  treemax       =  20000;  // tree

const
  // tree node field selectors
  t_op  = 0; t_op1 = 1;
  t_op2 = 2; t_op3 = 3;

  // symbols
  s_null     = 0;  s_name     = 1;  s_number = 2;
  s_lbracket = 3;  s_rbracket = 4;
  s_lparen   = 6;  s_rparen   = 7;

  s_fncall =  8;  s_pcall = 9;
  s_if     = 10;  s_then  = 11; s_else = 12;
  s_while  = 13;  s_do    = 14;
  s_ass    = 15;

  s_skip  = 16;
  s_begin = 17;  s_end = 18;

  s_semicolon = 19;  s_comma = 20;
  s_var       = 21;
  s_array     = 22;

  s_body = 23; s_proc = 24; s_func = 25;
  s_is   = 26;
  s_stop = 27;

  s_not = 32; s_neg = 34;
  s_val = 35; s_string = 36;

  s_true   = 42; s_false   = 43;
  s_return = 44; s_endfile = 60;

  s_diadic = 64;
  s_plus   = s_diadic +  0;  s_minus  = s_diadic +  1;
  s_or     = s_diadic +  5;  s_and    = s_diadic +  6;
  s_eq     = s_diadic + 10;  s_ne     = s_diadic + 11;
  s_ls     = s_diadic + 12;  s_le     = s_diadic + 13;
  s_gr     = s_diadic + 14;  s_ge     = s_diadic + 15;
  s_sub    = s_diadic + 16;

  v_nil = 0;

var
  // global packed and unpacked strings
  charv : wstring; charp    : word;
  wordv : wstring; wordsize : word; // !!! wordw - 0..wordsize

  // line buff
  linev : array [0..linemax-1] of word;
  linep, linelength : word;

  // char, val and symbol read
  ch     : word;
  numval : word;
  symbol : word;

  // hashed name table and lists in tree
  nametable : array [0..nametablesize-1] of word;
  tree      : array [0..treemax-1] of word;
  treep     : word;

  nullnode : word;
  zeronode : word;
  namenode : word;



  procedure rch;                          // !!! linev overflow to be fixed in xcomp.x
  procedure rdtag;
  procedure readnumber (base : word);
  function  readcharco : word;            // !!! eof to do
  procedure readstring;                   // !!! eof and strlengthmax to do

  // tree management
  function newvec (n : word)            : word;
  function cons1  (op : word)           : word;
  function cons2  (op,t1 : word)        : word;
  function cons3  (op, t1,t2 : word)    : word;
  function cons4  (op, t1,t2,t3 : word) : word;

  // name table lookup (wordv[0..wordmax]), returns symbol type, namenode
  // inserts as s_name if dont' exits
  function  lookupword : word;

  procedure declsyswords;
  procedure nextsymbol;



implementation

  procedure rch; // !!! linev overflow to be fixed in xcomp.x

    procedure rdline;
    begin
      linecount := linecount + 1;

      ch := getchar;
      linev [0] := ch;
      linelength := 1;
      while (ch <> eol) and (ch <> eof) and (linelength < linemax) do
      begin
        ch := getchar;
        linev [linelength] := ch;
        linelength := linelength + 1;
      end;
      linep := 0
    end;

  begin
    if linep >= linelength
    then rdline;
    ch := linev [linep];
    linep := linep + 1
  end;

  procedure rdtag; // !!! str overflow
  begin
    charp := 0;
    while ((ch >= ord ('A')) and  (ch <= ord ('Z'))) or
          ((ch >= ord ('a')) and  (ch <= ord ('z'))) or
          ((ch >= ord ('0')) and  (ch <= ord ('9'))) or
          (ch   = ord ('_')) do
    begin
      charp := charp + 1;
      charv [charp] := ch;
      rch
    end;
    charv [0] := charp;
    wordsize := packstring (charv, wordv)
  end;


  procedure readnumber (base : word);
  var d : word;

    function value (c:word) : word;
    begin
      if (c >= ord ('0')) and  (c <= ord ('9'))
      then value := c - ord ('0')
      else if (c >= ord ('A')) and (c <= ord ('Z'))
           then value := c - ord ('A') + 10
           else if (c >= ord ('a')) and (c <= ord ('z'))
                then value := c - ord ('a') + 10
                else value := 500
    end;

  begin
    d := value (ch);
    numval := 0;
    if d >= base
    then cmperror (ws('error in number'))
    else while d < base do
         begin
           numval := mul (numval, base) + d;
           rch;
           d := value (ch)
         end
  end;

  function readcharco : word; // !!! eof to do
  var v : word;
  begin
    if ch <> ord ('\')
    then v := ch
    else begin
           rch;
           case chr (ch) of
             '\','''', '"' : v := ch;
             'n' : v := eol;
             'r' : v := cr;
             't' : v := tab;
             else cmperror (ws('error in character constant'))
           end
         end;
    rch;
    readcharco := v
  end;

  procedure readstring; // !!! eof and string overflow to do
  var charc : word;
  begin
    charp := 0;
    while ch <> ord ('"') do
    begin
      if charp = eof
      then cmperror (ws('error in string constant'));
      charc := readcharco;
      charp := charp + 1;
      charv [charp] := charc
    end;
    charv [0] := charp;
    wordsize := packstring (charv, wordv)
  end;


  // tree node constructors
  function newvec (n : word) : word;
  var t : word;
  begin
    t := treep;
    treep := treep + n;
    if treep >= treemax // !!! >= change in xcomp.x something strange is going on
    then cmperror (ws('out of tree space'));
    newvec := t
  end;

  function cons1 (op : word) : word;
  var t : word;
  begin
    t := newvec (1);
    tree [t] := op;
    cons1 := t
  end;

  function cons2 (op,t1 : word) : word;
  var t : word;
  begin
    t := newvec (2);
    tree [t] := op; tree [t+1] := t1;
    cons2 := t
  end;

  function cons3 (op, t1,t2 : word) : word;
  var t : word;
  begin
    t := newvec (3);
    tree [t]   := op; tree [t+1] := t1;
    tree [t+2] := t2;
    cons3 := t
  end;

  function cons4 (op, t1,t2,t3 : word) : word;
  var t : word;
  begin
    t := newvec (4);
    tree [t]   := op; tree [t+1] := t1;
    tree [t+2] := t2; tree [t+3] := t3;
    cons4 := t
  end;



  function lookupword : word;
  var
    a, hashval : word;
    i, stype   : word;

    found, searching : boolean;

  begin
    a := wordv[0];
    hashval  := rem (a, nametablesize);
    namenode := nametable [hashval];

    found := false;
    searching := true;
    while searching do
      if namenode = v_nil
      then begin
             found     := false;
             searching := false
           end
      else begin
             i := 0;
             while (i <= wordsize) and (tree [namenode + i + 2] = wordv [i]) do
               i := i + 1;

             if i <= wordsize
             then namenode := tree [namenode + 1]
             else begin
                    stype     := tree [namenode];
                    found     := true;
                    searching := false
                  end
           end;
    if not found
    then begin
           namenode := newvec (wordsize + 3);
           tree [namenode]     := s_name;
           tree [namenode + 1] := nametable [hashval];
           i := 0;
           while i <= wordsize do
           begin
             tree [namenode + i + 2] := wordv [i];
             i := i + 1
           end;
           nametable [hashval] := namenode;
           stype := s_name;
         end;
    lookupword := stype
  end;



  procedure declsyswords;

    procedure declare (s : wstring; item : word);
    var l : word;
    begin
      unpackstring (s, charv);
      wordsize := packstring (charv, wordv);
      l := lookupword;
      tree [namenode] := item // replace s_name with item
    end;

  begin
    declare (ws('and'),    s_and);
    declare (ws('array'),  s_array);
    declare (ws('do'),     s_do);
    declare (ws('else'),   s_else);
    declare (ws('false'),  s_false);
    declare (ws('func'),   s_func);
    declare (ws('if'),     s_if);
    declare (ws('is'),     s_is);
    declare (ws('or'),     s_or);
    declare (ws('proc'),   s_proc);
    declare (ws('return'), s_return);
    declare (ws('skip'),   s_skip);
    declare (ws('stop'),   s_stop);
    declare (ws('then'),   s_then);
    declare (ws('true'),   s_true);
    declare (ws('val'),    s_val);
    declare (ws('var'),    s_var);
    declare (ws('while'),  s_while)
  end;



  procedure nextsymbol;
  var c : char;
  begin
    while (ch = eol) or (ch = cr) or (ch = ord (' ')) do rch;
    c := chr (ch);
    if c = '|'
    then begin
           rch;
           while ch <> ord ('|') do rch; // !!! eof
           rch;
           nextsymbol
         end
    else if ((c >= 'A') and  (c <= 'Z')) or
            ((c >= 'a') and  (c <= 'z'))
         then begin
                rdtag;
                symbol := lookupword
              end
         else if (c >= '0') and  (c <= '9')
              then begin
                     symbol := s_number;
                     readnumber (10)
                   end
              else case c of
                     '#' : begin rch; symbol := s_number; readnumber (16) end;
                     '[' : begin rch; symbol := s_lbracket end;
                     ']' : begin rch; symbol := s_rbracket end;
                     '(' : begin rch; symbol := s_lparen end;
                     ')' : begin rch; symbol := s_rparen end;
                     '{' : begin rch; symbol := s_begin end;
                     '}' : begin rch; symbol := s_end end;
                     ';' : begin rch; symbol := s_semicolon end;
                     ',' : begin rch; symbol := s_comma end;
                     '+' : begin rch; symbol := s_plus end;
                     '-' : begin rch; symbol := s_minus end;
                     '=' : begin rch; symbol := s_eq end;
                     '<' : begin
                             rch;
                             if ch <> ord ('=')
                             then symbol := s_ls
                             else begin rch; symbol := s_le end
                           end;
                     '>' : begin
                             rch;
                             if ch <> ord ('=')
                             then symbol := s_gr
                             else begin rch; symbol := s_ge end
                           end;
                     '~' : begin
                             rch;
                             if ch <> ord ('=')
                             then symbol := s_not
                             else begin rch; symbol := s_ne end
                           end;
                     ':' : begin
                             rch;
                             if ch <> ord('=')
                             then cmperror (ws('"=" expected'))
                             else begin rch; symbol := s_ass end
                           end;
                     '''': begin
                             rch;
                             numval := readcharco;
                             if ch <> ord ('''')
                             then cmperror (ws('error in character constant'))
                             else rch;
                             symbol := s_number
                           end;
                     '"' : begin
                             rch;
                             readstring;
                             if ch <> ord ('"')
                             then cmperror (ws('error in string constant'))
                             else rch;
                             symbol := s_string
                           end;
                      else begin
                             if ch = eof
                             then symbol := s_endfile
                             else cmperror (ws('illegal character'))
                           end
                   end
  end;


end.