unit synt;
interface
uses base;

  function formtree : word;

  function diadic  (op : word) : boolean;
  function monadic (op : word) : boolean;

implementation
uses
  lex;

  function diadic (op : word) : boolean;
  begin
     diadic := divf (op, s_diadic) <> 0
  end;

  function monadic (op : word) : boolean;
  begin
    monadic := (op = s_not) or (op = s_neg)
  end;

  procedure checkfor (s : word; m : wstring);
  begin
    if symbol = s
    then nextsymbol
    else cmperror (m)
  end;

  function rname : word;
  var a : word;
  begin
    if symbol <> s_name
    then cmperror (ws('name expected'))
    else begin
           a := namenode;
           nextsymbol
         end;
    rname := a
  end;

  function rexpression : word; forward;

  function rexplist : word;
  var a : word;
  begin
    a := rexpression;
    if symbol <> s_comma
    then rexplist := a
    else begin
           nextsymbol;
           rexplist := cons3 (s_comma, a, rexplist())
         end
  end;

  function relement : word;
  var a, b, i : word;
  begin
    if symbol = s_name
    then begin
           a := rname;
           if symbol = s_lbracket
           then begin
                  nextsymbol;
                  b := rexpression;
                  checkfor (s_rbracket, ws('"]" expected'));
                  a := cons3 (s_sub, a, b)
                end
           else if symbol = s_lparen
                then begin
                       nextsymbol;
                       if symbol = s_rparen
                       then b := nullnode
                       else b := rexplist;
                       checkfor (s_rparen, ws('")" expected'));
                       a := cons3 (s_fncall, a, b)
                     end
         end
    else if symbol = s_number
         then begin
                a := cons2 (s_number, numval);
                nextsymbol
              end
         else if (symbol = s_true) or (symbol = s_false)
              then begin
                     a := namenode;
                     nextsymbol
                   end
              else if symbol = s_string
                   then begin
                          a := newvec (wordsize + 2);
                          tree [a + t_op] := s_string;
                          i := 0;
                          while i <= wordsize do
                          begin
                            tree [a + i + 1] := wordv [i];
                            i := i + 1
                          end;
                          nextsymbol
                        end
                   else if symbol <> s_lparen
                        then cmperror (ws('error in expression'))
                        else begin
                               nextsymbol;
                               a := rexpression;
                               checkfor (s_rparen, ws('")" expected'))
                             end;
    relement := a
  end;

  function associative (s : word) : boolean;
  begin
    associative := (s = s_and) or (s = s_or) or (s = s_plus)
  end;

  function rright (s : word) : word;
  var b : word;
  begin
    b := relement;
    if not (associative (s) and (symbol = s))
    then rright := b
    else begin
           nextsymbol;
           rright := cons3 (s, b, rright (s))
         end
  end;

  function rexpression : word;
  var a,b,s : word;
  begin
    if symbol = s_minus
    then begin
           nextsymbol;
           b := relement;
           rexpression := cons2 (s_neg, b)
         end
    else if symbol = s_not
         then begin
                nextsymbol;
                b := relement;
                rexpression := cons2 (s_not, b)
              end
         else begin
                a := relement;
                if not diadic (symbol)
                then rexpression := a
                else begin
                       s := symbol;
                       nextsymbol;
                       rexpression := cons3 (s, a, rright (s))
                     end
              end
  end;



  function rdecl : word;
  var a, b : word;
  begin
    if symbol = s_var
    then begin
           nextsymbol;
           a := cons2 (s_var, rname)
         end
    else if symbol = s_array
         then begin
                nextsymbol;
                a := rname;
                checkfor (s_lbracket, ws('"[" expected'));
                b := rexpression;
                checkfor (s_rbracket, ws('"]" expected'));
                a := cons3 (s_array, a, b)
              end
         else if symbol = s_val
              then begin
                     nextsymbol;
                     a := rname;
                     checkfor (s_eq, ws('"=" expected'));
                     b := rexpression;
                     a := cons3 (s_val, a, b);
                   end;
    checkfor (s_semicolon, ws('";" expected'));
    rdecl := a
  end;

  function rgdecls : word;
  var a : word;
  begin
    a := rdecl;
    if (symbol = s_val) or (symbol = s_var) or (symbol = s_array)
    then rgdecls := cons3 (s_semicolon, a, rgdecls())
    else rgdecls := a
  end;

  function rldecls : word;
  var a : word;
  begin
    a := rdecl;
    if (symbol = s_val) or (symbol = s_var)
    then rldecls := cons3 (s_semicolon, a, rldecls())
    else rldecls := a
  end;



  function rformals : word;
  var s,a,b : word;
  begin
    if (symbol = s_val) or (symbol = s_array) or (symbol = s_proc) or (symbol = s_func)
    then begin
           s := symbol;
           nextsymbol;
           if symbol = s_name
           then a := cons2 (s, rname)
           else cmperror (ws('name expected'))
         end;
    if symbol <> s_comma
    then rformals := a
    else begin
           nextsymbol;
           b := rformals ();
           rformals := cons3 (s_comma, a, b)
         end
  end;

  function rstatement : word; forward;

  function rprocdecl : word;
  var s,a,b,c : word;
  begin
    s := symbol;
    nextsymbol;
    a := rname ();
    checkfor (s_lparen, ws('"(" expected'));
    if symbol = s_rparen
    then b := nullnode
    else b := rformals;
    checkfor (s_rparen, ws('")" expected'));
    checkfor (s_is, ws('"is" expected'));
    if (symbol = s_var) or (symbol = s_val)
    then c := rldecls
    else c := nullnode;
    c := cons3 (s_body, c, rstatement);
    rprocdecl := cons4 (s, a, b, c)
  end;

  function rprocdecls : word;
  var a : word;
  begin
    a := rprocdecl;
    if (symbol = s_proc) or (symbol = s_func)
    then rprocdecls := cons3 (s_semicolon, a, rprocdecls ())
    else rprocdecls := a
  end;

  function rstatements : word;
  var a : word;
  begin
    a := rstatement;
    if symbol <> s_semicolon
    then rstatements := a
    else begin
           nextsymbol;
           rstatements := cons3 (s_semicolon, a, rstatements ())
         end
  end;

  function rstatement : word;
  var a,b,c : word;
  begin
    if symbol = s_skip
    then begin
           nextsymbol;
           rstatement := cons1 (s_skip)
         end
    else if symbol = s_stop
         then begin
                nextsymbol;
                rstatement := cons1 (s_stop)
              end
         else if symbol = s_return
              then begin
                     nextsymbol;
                     rstatement := cons2 (s_return, rexpression)
                   end
              else if symbol = s_if
                    then begin
                           nextsymbol;
                           a := rexpression;
                           checkfor (s_then, ws('"then" expected'));
                           b := rstatement ();
                           checkfor (s_else, ws('"else" expected'));
                           c := rstatement ();
                           rstatement := cons4 (s_if, a, b, c)
                         end
                    else if symbol = s_while
                         then begin
                                nextsymbol;
                                a := rexpression;
                                checkfor (s_do, ws('"do" expected'));
                                b := rstatement ();
                                rstatement := cons3 (s_while, a, b)
                              end
                         else if symbol = s_begin
                              then begin
                                     nextsymbol;
                                     a := rstatements;
                                     checkfor (s_end, ws('"}" expected'));
                                     rstatement := a
                                   end
                              else if symbol = s_name
                                   then begin
                                         a := relement;
                                         if tree [a + t_op] = s_fncall
                                         then begin
                                                tree [a + t_op] := s_pcall;
                                                rstatement := a
                                              end
                                         else begin
                                                checkfor (s_ass, ws('":=" expected'));
                                                rstatement := cons3 (s_ass, a, rexpression ())
                                              end
                                        end
                                   else begin
                                          cmperror (ws('error in command'));
                                          rstatement := cons1 (s_stop)
                                        end
  end;



  function formtree : word;
  var i,t : word;
  begin
    i := 0;
    while i < nametablesize do
    begin
      nametable[i] := v_nil;
      i := i + 1
    end;
    treep := 1; // v_nil is zero, tree starts from 1

    charp := 0;

    declsyswords;

    nullnode := cons1 (s_null);
    zeronode := cons2 (s_number, 0);

    linecount := 0;

    linelength := 0;
    linep      := 1;
    rch;

    nextsymbol;
    if (symbol = s_var) or (symbol = s_val) or (symbol = s_array)
    then t := rgdecls
    else t := nullnode;

    formtree := cons3 (s_body, t, rprocdecls)
  end;


end.