unit z_debug;
interface
uses base, lex, synt;

  function  sym_name (s:word) : string;
  procedure treestr (p:word);
  procedure outnames;

implementation

  function sym_name (s:word) : string;
  begin
    case s of
      s_null      : sym_name := 'null';
      s_name      : sym_name := 'name';
      s_number    : sym_name := 'number';
      s_lbracket  : sym_name := 'lbracket';
      s_rbracket  : sym_name := 'rbracket';
      s_lparen    : sym_name := 'lparen';
      s_rparen    : sym_name := 'rparen';
      s_fncall    : sym_name := 'fncall';
      s_pcall     : sym_name := 'pcall';
      s_if        : sym_name := 'if';
      s_then      : sym_name := 'then';
      s_else      : sym_name := 'else';
      s_while     : sym_name := 'while';
      s_do        : sym_name := 'do';
      s_ass       : sym_name := 'ass';
      s_skip      : sym_name := 'skip';
      s_begin     : sym_name := 'begin';
      s_end       : sym_name := 'end';
      s_semicolon : sym_name := 'semicolon';
      s_comma     : sym_name := 'coma';
      s_var       : sym_name := 'var';
      s_array     : sym_name := 'array';
      s_body      : sym_name := 'body';
      s_proc      : sym_name := 'proc';
      s_func      : sym_name := 'func';
      s_is        : sym_name := 'is';
      s_stop      : sym_name := 'stop';
      s_not       : sym_name := 'not';
      s_neg       : sym_name := 'neg';
      s_val       : sym_name := 'val';
      s_string    : sym_name := 'string';
      s_true      : sym_name := 'true';
      s_false     : sym_name := 'false';
      s_return    : sym_name := 'return';
      s_endfile   : sym_name := 'endfile';
      s_plus      : sym_name := 'plus';
      s_minus     : sym_name := 'minus';
      s_or        : sym_name := 'or';
      s_and       : sym_name := 'and';
      s_eq        : sym_name := 'eq';
      s_ne        : sym_name := 'ne';
      s_ls        : sym_name := 'ls';
      s_le        : sym_name := 'le';
      s_gr        : sym_name := 'gr';
      s_ge        : sym_name := 'ge';
      s_sub       : sym_name := 'sub';
      else  sym_name := 'unknown'
    end
  end;


  procedure treestr (p:word);
  var l,lw : word;
  begin
    l := tree[p] mod 256 + 1;
    lw := (l-1) div bytesperword + 1;
    for l := 0 to lw-1 do
      wordv[l] := tree[p+l];
    prints (wordv)
  end;

  procedure outnames;
  var i,p : word;

    procedure outsym (p:word);
    begin
      write ('[',sym_name(tree[p]),']');
      treestr (p+2);
      write (' ')
    end;

  begin
    writeln ('name table: ');
    for i := 0 to nametablesize-1 do
    begin
      p := nametable[i];
      if p<>v_nil
      then begin
             write (i,': ');
             while p<>v_nil do
             begin
               outsym (p);
               p := tree[p+1]
             end;
             writeln
           end
    end
  end;

end.