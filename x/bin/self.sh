#!/bin/bash
fpc -Fuunits xhex
fpc hex
xhex # creates xhex.bin
hex xhex # run xhex.bin with xhex.x as input generating f2.out
hex      # run f2.out with xhex.x generating f2.out
diff -s f2.out xhex.bin