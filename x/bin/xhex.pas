program xhex;
uses base, lex, synt;

const
  maxaddr     = 200000;
  cb_size     =  15000;
  labval_size =   2000;

// code buffer flags
const
  pflag   =           $1000;
  cb_flag = word ($10000000);
  cb_high =        $1000000;

  cbf_inst   =  1;  cbf_lab    =  2;  cbf_fwdref =  3;  cbf_bwdref =  4;
  cbf_stack  =  5;  cbf_const  =  6;  cbf_string =  7;  cbf_entry  =  8;
  cbf_pexit  =  9;  cbf_fnexit = 10;  cbf_var    = 11;  cbf_constp = 12;


var
  codebuffer : array [0..cb_size] of word;

  cbv_flag, cbv_high, cbv_low : word;

  cb_bufferp, cb_entryinstp,
  cb_loadpoint, cb_conststart,
  cb_stringstart : word;

  entrylab : word;

// name scoping stack
var
  names_d : array [0..500] of word;
  names_v : array [0..500] of word;
  namep, nameb : word;

// generator
const
  i_ldam =  0;  i_ldbm =  1;  i_stam =  2;  i_ldac =  3;
  i_ldbc =  4;  i_ldap =  5;  i_ldai =  6;  i_ldbi =  7;
  i_stai =  8;  i_br   =  9;  i_brz  = 10;  i_brn  = 11;
  i_opr  = 13;  i_pfix = 14;  i_nfix = 15;

  o_brb  = 0;   o_add  =  1;  o_sub  =  2;  o_svc  =  3;

  r_areg = 0;   r_breg =  1;
  m_sp   = 1;

var
  arrayspace : word;
  //, arraycount : word;
  codesize,   procdef    : word;
  proclabel              : word;

  infunc : boolean;

  stackp, stk_max : word;


// constants; strings and labels
var
  consts : array [0..500] of word;
  constp : word;

  strings : array [0..1000] of word;
  stringp : word;

  labval : array [0..labval_size] of word;
  labelcount : word;



  procedure namemessage (ex:word; s : wstring; x : word);
  var n,p,w,l,b : word;
  begin
    prints (s);
    if tree [x + t_op] = s_name
    then begin
           n := 1;
           p := 2;
           w := tree [x + p];
           l := rem  (w, 256);
           w := divf (w, 256);
           b := 1;
           while n <= l do
           begin
             putval (rem (w, 256));
             w := divf (w, 256);
             n := n + 1;
             b := b + 1;
             if b = bytesperword
             then begin
                    b := 0;
                    p := p + 1;
                    w := tree [x + p]
                  end
           end
         end;
    newline;
    if ex=1 then done (1)
  end;

  procedure generror (s:wstring);
  begin
    prints (s);
    newline;
    namemessage (1, ws('in function '), tree [procdef + t_op1])
  end;

  procedure addname (x,v : word);
  begin
    names_d [namep] := x;
    names_v [namep] := v;
    namep := namep + 1
  end;

  function findname (x : word) : word;
  var n : word; found : boolean;
  begin
    found := false;
    n := namep - 1;
    while (found = false) and (n >= 0) do
      if tree [names_d[n] + t_op1] = x
      then found := true
      else n := n - 1;
    if not found
    then begin
           namemessage (0,ws('name not declared '), x);
           namemessage (1,ws('in function '), tree [procdef + t_op1])
         end;
    findname := n
  end;



  procedure stk_init (n:word);
  begin
    stackp  := n;
    stk_max := n
  end;

  function getlabel : word;
  begin
    if labelcount < labval_size
    then labelcount := labelcount + 1
    else generror (ws('too many labels'));
    getlabel := labelcount
  end;

  procedure gen (t,h,l : word);
  begin
    cb_loadpoint := cb_loadpoint + 1;
    codebuffer [cb_bufferp] := mul2 (t, cb_flag) + mul2 (h, cb_high) + l + 65536;
    cb_bufferp := cb_bufferp + 1;
    if cb_bufferp = cb_size
    then generror (ws('code buffer overflow'))
  end;

  procedure geni (i, opd : word);
  begin
    gen (cbf_inst, i, opd)
  end;

  procedure genref (inst, lab : word);
  begin
    if labval [lab] = 0
    then gen (cbf_fwdref, inst, lab)
    else gen (cbf_bwdref, inst, lab)
  end;

  procedure gensref (i, offs : word);
  begin
    gen (cbf_stack, i, offs)
  end;

  procedure genbr (seq : boolean; lab : word);
  begin
    if not seq
    then genref (i_br, lab)
  end;

  function genconst (n:word) : word;
  var i, cp : word; found : boolean;
  begin
    found := false;
    i := 0;
    while (i < constp) and (found = false) do
      if consts[i] <> n
      then i := i+1
      else begin
             found := true;
             cp := i
           end;
      if not found
      then begin
             consts[constp] := n;
             cp := constp;
             constp := constp + 1
           end;
    genconst := cp
  end;

  procedure genstring (x:word);
  var i, sp : word;
  begin
    sp := stringp;
    i := 0;
    while i <= divf (rem (tree[x + 1], 256), 4) do
    begin
      strings [stringp] := tree [x + i + 1];
      stringp := stringp + 1;
      i := i + 1
    end;
    gen (cbf_string, 0, sp)
  end;

  procedure genentry;
  begin
    cb_entryinstp := cb_bufferp;
    gen (cbf_entry, 0, 0)
  end;

  procedure genexit;

    procedure cb_setlow (p,f : word);
    var t : word;
    begin
      // $80010000 div $1000000
      // writeln (p,' ',f,' ',codebuffer[p],' ',cb_high,' ',codebuffer[p] div cb_high);
      t := divf (codebuffer[p], cb_high);
      codebuffer[p] := mul2 (t, cb_high) + f + 65536
    end;

  begin
    cb_setlow (cb_entryinstp, stk_max);
    if tree[procdef + t_op] = s_proc
    then gen (cbf_pexit,  0, 0)
    else gen (cbf_fnexit, 0, 0)
  end;

  procedure genlab (l : word);
  begin
    labval [l] := cb_loadpoint;
    gen (cbf_lab, 0, l)
  end;



  function getval (x : word) : word;
  var op : word;
  begin
    op := tree [x + t_op];
    if op = s_true
    then getval := 1
    else if op = s_false
         then getval := 0
         else if op = s_number
              then getval := tree[x + t_op1]
              else getval := 0
  end;

  function isval (x : word) : boolean;
  var op : word;
  begin
    op := tree [x + t_op];
    isval := (op = s_true) or (op = s_false) or (op = s_number)
  end;

  function iszero (x : word) : boolean;
  begin
    iszero := isval (x) and (getval (x) = 0)
  end;

  function optimiseexpr (x:word) : word;
  var
    r, op : word;

    left,   right   : word;
    leftop, rightop : word;
    name,   temp    : word;

    // boolean represented by 0 and 1
    function evalmonadic (x:word) : word;
    var op, opd : word;
    begin
      op  := tree [x + t_op];
      opd := getval (tree [x + t_op1]);
      case op of
        s_neg : evalmonadic :=  - opd;
        s_not : evalmonadic := 1 - opd;
        else    generror (ws('compiler error'))
      end
    end;

    function evaldiadic (x:word) : word;
    var op, left, right : word;
    begin
      op := tree [x + t_op];
      left  := getval (tree [x + t_op1]);
      right := getval (tree [x + t_op2]);
      case op of
        s_plus  : evaldiadic := left + right;
        s_minus : evaldiadic := left - right;
        s_eq    : evaldiadic := ord (left  = right);
        s_ne    : evaldiadic := ord (left <> right);
        s_ls    : evaldiadic := ord (left  < right);
        s_gr    : evaldiadic := ord (left  > right);
        s_le    : evaldiadic := ord (left <= right);
        s_ge    : evaldiadic := ord (left >= right);
        s_or    : evaldiadic := left  or right;
        s_and   : evaldiadic := left and right;
        else      cmperror (ws('optimise error'))
      end
    end;

  begin
    r  := x;
    op := tree [x + t_op];
    if op = s_name
    then begin
           name := findname (x);
           if tree [names_d[name] + t_op] = s_val
           then r := tree [names_d[name] + t_op2]
         end
    else if monadic (op)
         then begin
                tree [x + t_op1] := optimiseexpr (tree[x + t_op1]);
                if isval (tree[x + t_op1])
                then begin
                       tree [x + t_op1] := evalmonadic (x);
                       tree [x + t_op]  := s_number
                     end
                else if op = s_neg
                     then r := cons3 (s_minus, zeronode, tree [x + t_op1])
              end
        else if op = s_fncall
             then begin
                    tree [x + t_op2] := optimiseexpr (tree [x + t_op2]);
                    tree [x + t_op1] := optimiseexpr (tree [x + t_op1])
                  end
             else if diadic (op)
                  then begin
                         tree [x + t_op2] := optimiseexpr (tree [x + t_op2]);
                         tree [x + t_op1] := optimiseexpr (tree [x + t_op1]);
                         left  := tree [x + t_op1];
                         right := tree [x + t_op2];
                         leftop  := tree [left  + t_op];
                         rightop := tree [right + t_op];
                         if op = s_sub
                         then
                         else if isval (left) and isval (right)
                              then begin
                                     tree [x + t_op1] := evaldiadic (x);
                                     tree [x + t_op]  := s_number
                                   end
                              else if op = s_eq
                                   then if (leftop = s_not) and (rightop = s_not)
                                        then begin
                                               tree [x + t_op1] := tree [left  + t_op1];
                                               tree [x + t_op2] := tree [right + t_op1]
                                             end
                                        else
                                   else if op = s_ne
                                        then begin
                                               tree[x + t_op] := s_eq;
                                               r := cons2 (s_not, x);
                                               if (leftop = s_not) and (rightop = s_not)
                                               then begin
                                                     tree [x + t_op1] := tree [left  + t_op1];
                                                     tree [x + t_op2] := tree [right + t_op1]
                                                    end
                                               else

                                             end
                                        else if op = s_ge
                                             then begin
                                                    tree [x + t_op] := s_ls;
                                                    r := cons2 (s_not, x)
                                                  end
                                             else if op = s_gr
                                                  then begin
                                                         temp := tree [x + t_op1];
                                                         tree [x + t_op1] := tree [x + t_op2];
                                                         tree [x + t_op2] := temp;
                                                         tree [x + t_op]  := s_ls
                                                       end
                                                  else if op = s_le
                                                       then begin
                                                              temp := tree [x + t_op1];
                                                              tree [x + t_op1] := tree [x + t_op2];
                                                              tree [x + t_op2] := temp;
                                                              tree [x + t_op]  := s_ls;
                                                              r := cons2 (s_not, x)
                                                            end
                                                       else if (op = s_or) or (op = s_and)
                                                            then if (leftop = s_not) and (rightop = s_not)
                                                                 then begin
                                                                        r := cons2 (s_not, x);
                                                                        if tree [x + t_op] = s_and
                                                                        then tree [x + t_op] := s_or
                                                                        else tree [x + t_op] := s_and;
                                                                        tree [x + t_op1] := tree [left  + t_op1];
                                                                        tree [x + t_op2] := tree [right + t_op1]
                                                                      end
                                                                 else
                                                            else if ((op = s_plus) or (op = s_or)) and
                                                                     (iszero (tree[x + t_op1]) or iszero (tree[x + t_op2]))
                                                                 then if iszero (tree [x + t_op1])
                                                                      then r := tree [x + t_op2]
                                                                      else if iszero (tree [x + t_op2])
                                                                           then r := tree [x + t_op1]
                                                                           else
                                                                 else if (op = s_minus) and iszero (tree [x + t_op2])
                                                                      then r := tree[x + t_op1]
                                                                      else
                       end
                  else if op = s_comma
                       then begin
                              tree[x + t_op2] := optimiseexpr (tree[x + t_op2]);
                              tree[x + t_op1] := optimiseexpr (tree[x + t_op1])
                            end
                       else;
    optimiseexpr := r
  end;



  procedure declglobals (x:word);
  var op : word;
  begin
    op := tree[x + t_op];
    case op of
      s_semicolon : begin
                      declglobals (tree[x + t_op1]);
                      declglobals (tree[x + t_op2])
                    end;
      s_var       : begin
                      addname (x, stackp);
                      stackp := stackp + 1
                    end;
      s_val       : begin
                      tree [x + t_op2] := optimiseexpr (tree [x + t_op2]);
                      if isval (tree [x + t_op2])
                      then addname (x, getval (tree [x + t_op2]))
                      else generror (ws('constant expression expected'))
                    end;
      s_array     : begin
                      tree [x + t_op2] := optimiseexpr (tree [x + t_op2]);
                      if isval (tree [x + t_op2])
                      then begin
                             arrayspace := arrayspace + getval (tree [x + t_op2]);
                             addname (x, stackp);
                             stackp := stackp + 1
                           end
                      else generror (ws('constant expression expected'))
                    end
    end
  end;

  procedure tglobals;
  var g, arraybase, name : word;
  begin
    g := 0;
    arraybase := maxaddr - arrayspace;
    gen (cbf_var, 0, arraybase - 2);
    while g < namep do
    begin
      name := names_d [g];
      if tree [name + t_op] = s_array
      then begin
             gen (cbf_var, 0, arraybase);
             arraybase := arraybase + getval (tree[name + t_op2])
           end
      else if tree[name + t_op] = s_var
           then gen (cbf_var, 0, 0);
      g := g + 1
    end
  end;

  procedure declprocs (x:word);
  begin
    if tree [x + t_op] <> s_semicolon
    then addname (x, getlabel)
    else begin
           declprocs (tree [x + t_op1]);
           declprocs (tree [x + t_op2])
         end
  end;

  procedure declformals (x:word);
  var op : word;
  begin
    op := tree[x + t_op];
    if op <> s_null
    then if op = s_comma
         then begin
                declformals (tree[x + t_op1]);
                declformals (tree[x + t_op2])
              end
         else begin
                if op = s_val
                then tree [x + t_op] := s_var;

                addname (x, stackp + pflag);
                stackp := stackp + 1
              end
  end;

  procedure decllocals (x:word);
  var op : word;
  begin
    op := tree [x + t_op];
    if op <> s_null
    then if op = s_semicolon
         then begin
                decllocals (tree [x + t_op1]);
                decllocals (tree [x + t_op2])
              end
         else if op = s_var
              then begin
                     addname (x, stackp);
                     stackp := stackp + 1
                   end
              else if op = s_val
                   then begin
                          tree [x + t_op2] := optimiseexpr (tree[x + t_op2]);
                          if isval (tree[x + t_op2])
                          then addname (x, getval (tree[x + t_op2]))
                          else generror (ws('constant expression expected'))
                        end
  end;



  procedure genjump (inst:word; cond:boolean; target:word);
  var lab : word;
  begin
    if cond
    then genref (inst, target)
    else begin
           lab := getlabel;
           genref (inst, lab);
           genref (i_br, target);
           genlab (lab)
         end
  end;



  procedure loadconst (reg, value : word);
  begin
    if not ((value > (- 65536)) and (value < 65536))
    then gen (cbf_const, reg, genconst (value))
    else if reg = r_areg
         then geni (i_ldac, value)
         else geni (i_ldbc, value)
  end;

  function islocal (n:word) : boolean;
  begin
    islocal := n >= nameb
  end;

  procedure loadvar (reg, vn : word);
  var offs : word;
  begin
    offs := names_v [vn];
    if not islocal (vn)
    then if reg = r_areg
         then geni (i_ldam, offs)
         else geni (i_ldbm, offs)
    else if reg = r_areg
         then begin
                geni    (i_ldam, m_sp);
                gensref (i_ldai, offs)
              end
         else begin
                geni    (i_ldbm, m_sp);
                gensref (i_ldbi, offs)
             end
  end;

  procedure gencall (entry, actuals : word);
  var link, def : word;

    // !!! check number of params here
    procedure checkps (alist, flist : word);
    var ax, fx : word;

      // !!! check param types here
      procedure checkp (a, f : word);
      begin
        if tree[f + t_op] = s_null
        then
        else if tree[f + t_op] = s_val
             then
             else if tree[f + t_op] = s_array
                  then
                  else if tree[f + t_op] = s_proc
                       then
                       else
      end;

    begin // !!! put and get l0 and l1?
      ax := alist;
      fx := flist;
      while tree [fx + t_op] = s_comma do // !!! not checking if one param, is it intentional for put and get ?
        if tree [ax + t_op] <> s_comma
        then cmperror (ws('parameter mismatch'))
        else begin
               checkp (tree [ax + t_op1], tree [fx + t_op1]);
               fx := tree [fx + t_op2];
               ax := tree [ax + t_op2]
             end;
      checkp (ax, fx)
    end;

  begin
    link := getlabel;
    genref (i_ldap, link);
    if islocal (entry)
    then begin
          loadvar (r_breg, entry);
          geni    (i_opr,  o_brb)
         end
    else begin
           def := names_d [entry];
           checkps (tree[def + t_op2], actuals);
           genref (i_br, names_v[entry])
         end;
    genlab (link)
  end;

  procedure loadbase (reg,base : word);
  var name, def : word;
  begin
    if isval (base)
    then loadconst (reg, getval (base))
    else begin
           name := findname (base);
           def  := names_d [name];
           if tree[def + t_op] = s_array
           then loadvar (reg, name)
           else namemessage (1,ws('array expected'), tree [def + t_op1])
         end
  end;



  procedure setstack;
  begin
    if stk_max < stackp
    then stk_max := stackp
  end;

  procedure texp  (x : word);                 forward;
  procedure tbool (x : word; cond : boolean); forward;

  // op = s_plus or s_minus
  procedure texp2 (op, op1, op2 : word);
  var left, right, sp : word;

    function needsareg (x:word) : boolean;
    var op : word;
    begin
      op := tree [x + t_op];
      needsareg := not (isval (x) or (op = s_string) or (op = s_name))
    end;

    function regsfor (x:word) : word;
    var op, rleft, rright : word;
    begin
      op := tree [x + t_op];
      if op = s_fncall
      then regsfor := 10
      else if monadic (op)
           then regsfor := regsfor (tree [x + t_op1])
           else if diadic (op)
                then begin
                       rleft  := regsfor (tree [x + t_op1]);
                       rright := regsfor (tree [x + t_op2]);
                       if rleft = rright
                       then regsfor :=  1 + rleft
                       else if rleft > rright
                            then regsfor :=  rleft
                            else regsfor :=  rright
                     end
                else regsfor := 1
    end;

    procedure tbexp (x:word);
    var op, left, value, def : word;
    begin
      op := tree [x + t_op];
      if isval (x)
      then begin
             value := getval (x);
             loadconst (r_breg, value)
           end
      else if op = s_string
           then genstring (x)
           else if op = s_name
                then begin
                       left := findname (x);
                       def  := names_d [left];
                       if tree [def + t_op] = s_val
                       then loadconst (r_breg, names_v [left])
                       else if tree [def + t_op] = s_var
                            then loadvar (r_breg, left)
                     end
    end;

  begin
    left  := op1;
    right := op2;
    if (op = s_plus) and (regsfor (left) < regsfor (right))
    then begin
           left  := op2;
           right := op1
         end;

    if not needsareg (right)
    then begin
           texp  (left);
           tbexp (right)
         end
    else begin
           sp := stackp;
           texp   (right);
           stackp := stackp + 1;
           setstack;
           geni    (i_ldbm, m_sp);
           gensref (i_stai,   sp);

           texp    (left);
           geni    (i_ldbm, m_sp);
           gensref (i_ldbi,   sp);
           stackp := sp
         end;

    if op = s_plus
    then geni (i_opr, o_add)
    else if op = s_minus
         then geni (i_opr, o_sub)
  end;

  procedure tcall (x : word; seq : boolean; clab : word; tail : boolean);
  var sp, entry, actuals, def : word;

    procedure tactuals (aps, n : word);
    var sp : word;

      function containscall (x:word) : boolean;
      var op : word;
      begin
        op := tree [x + t_op];
        if op = s_null
        then containscall := false
        else if monadic (op)
             then containscall := containscall (tree [x + t_op1])
             else if diadic (op)
                  then containscall := containscall (tree [x + t_op1]) or containscall (tree [x + t_op2])
                  else containscall := op = s_fncall
      end;

      procedure  preparecalls (x:word);

        procedure preparecall (x:word);
        var op : word;
        begin
          op := tree [x + t_op];
          if op <> s_null
          then if containscall (x)
               then begin
                      sp := stackp;
                      texp (x);
                      stackp := stackp + 1;
                      setstack;
                      geni    (i_ldbm, m_sp);
                      gensref (i_stai,   sp)
                    end
        end;

      begin
        if tree[x + t_op] <> s_comma
        then preparecall (x)
        else begin
               preparecalls (tree[x + t_op2]);
               preparecall  (tree[x + t_op1])
             end
      end;

      procedure loadaps (x, n : word);

        procedure loadap (x, n : word);
        var op, vn, aptype : word;
        begin
          op := tree [x + t_op];
          if op <> s_null
          then if not containscall (x)
               then begin
                      if op <> s_name
                      then texp (x)
                      else begin
                             vn := findname (x);
                             aptype := tree [names_d [vn] + t_op];
                             if aptype = s_val
                             then loadconst (r_areg, names_v [vn])
                             else if aptype <> s_func
                                  then loadvar (r_areg, vn)
                                  else if islocal (vn)
                                       then loadvar (r_areg, vn)
                                       else genref  (i_ldap, names_v [vn])
                           end;
                      geni (i_ldbm, m_sp);
                      geni (i_stai,    n)
                    end
        end;

      begin
        if tree [x + t_op] <> s_comma
        then loadap(x, n)
        else begin
               loadaps (tree [x + t_op2], n + 1);
               loadap  (tree [x + t_op1], n)
             end
      end;

      function numps (x:word) : word;
      begin
        if tree [x + t_op] = s_null
        then numps := 0
        else if tree [x + t_op] <> s_comma
             then numps := 1
             else numps := 1 + numps (tree[x + t_op2])
      end;

      procedure loadcalls (x, n : word);

        procedure loadcall (x, n : word);
        var op : word;
        begin
          op := tree [x + t_op];
          if op <> s_null
          then if containscall (x)
               then begin
                      geni    (i_ldam,   m_sp);
                      gensref (i_ldai, stackp);
                      stackp := stackp + 1;
                      geni    (i_ldbm,   m_sp);
                      geni    (i_stai,      n)
                    end
        end;

      begin
        if tree [x + t_op] <> s_comma
        then loadcall(x, n)
        else begin
               loadcalls (tree [x + t_op2], n + 1);
               loadcall  (tree [x + t_op1], n)
             end
      end;

    begin
      sp := stackp;
      preparecalls (aps);
      loadaps (aps, n);
      stackp := stackp + numps (aps) + n;
      setstack;
      stackp := sp;
      loadcalls (aps, n);
      stackp := sp
    end;

  begin
    sp := stackp;
    actuals := tree [x + t_op2];
    if isval (tree[x + t_op1])
    then begin
           tactuals (actuals, 2);
           texp (tree [x + t_op1]);
           geni (i_opr,  o_svc);
           geni (i_ldam,  m_sp);
           geni (i_ldai,     1)
         end
    else begin
           entry := findname (tree[x + t_op1]);
           def := names_d [entry];
           if tree [def + t_op] = s_func
           then begin
                  tactuals (actuals,     2);
                  gencall  (entry, actuals);
                  geni     (i_ldai,      1)
                end
           else begin
                  tactuals (actuals,     1);
                  gencall  (entry, actuals)
                end;
           genbr (seq, clab)
         end;
    stackp := sp
  end;

  procedure texp (x:word);
  var
    op, left   : word;
    value, def : word;

  begin
    op := tree [x + t_op];
    if isval (x)
    then begin
           value := getval (x);
           loadconst (r_areg, value)
         end
    else begin
           if op = s_string
           then genstring (x)
           else if op = s_name
                then begin
                       left := findname (x);
                       def := names_d [left];
                       if tree [def + t_op] = s_val
                       then loadconst (r_areg, names_v [left])
                       else if tree [def + t_op] = s_var
                            then loadvar (r_areg, left)
                     end
                else if (op = s_not) or (op = s_and) or (op = s_or) or
                        (op = s_eq)  or (op = s_ls)
                     then tbool (x, true)
                     else if op <> s_sub
                          then if op = s_fncall
                               then tcall (x, true, 0, false)
                               else texp2 (op, tree[x + t_op1], tree[x + t_op2])
                          else begin
                                 left := tree[x + t_op1];
                                 def := names_d[left];
                                 if isval (tree[x + t_op2])
                                 then begin
                                        loadbase (r_areg, left);
                                        value := getval (tree[x + t_op2]);
                                        geni (i_ldai, value)
                                      end
                                 else begin
                                        texp (tree[x + t_op2]);
                                        loadbase (r_breg, left);
                                        geni (i_opr, o_add);
                                        geni (i_ldai, 0)
                                      end
                               end
         end
  end;

  procedure genassign (left,right : word);
  var
    sp, leftop    : word;
    name, base    : word;
    offset, value : word;

    procedure storevar (vn : word);
    var offs : word;
    begin
      offs := names_v [vn];
      if not islocal (vn)
      then geni (i_stam, offs)
      else begin
             geni    (i_ldbm, m_sp);
             gensref (i_stai, offs)
           end
    end;

  begin
    leftop := tree [left + t_op];
    if leftop = s_name
    then begin
           name := findname (left);
           texp (right);
           storevar (name)
         end
    else begin
           base   := tree [left + t_op1];
           offset := tree [left + t_op2];
           if isval (offset)
           then begin
                  value := getval (offset);
                  texp (right);
                  loadbase (r_breg, base);
                  geni (i_stai, value)
                end
           else begin
                  sp := stackp;
                  texp (offset);
                  loadbase (r_breg, base);
                  geni    (i_opr,  o_add);
                  stackp := stackp + 1;
                  setstack;
                  geni    (i_ldbm,  m_sp);
                  gensref (i_stai,    sp);
                  texp (right);
                  geni    (i_ldbm,  m_sp);
                  gensref (i_ldbi,    sp);
                  geni    (i_stai,     0);
                  stackp := sp
                end
         end
  end;

  procedure gencondjump (x:word; cond:boolean; target : word);
  var op, lab : word;
  begin
    op := tree [x + t_op];
    case op of
      s_not : gencondjump (tree[x + t_op1], not cond, target);
      s_and,
      s_or  : if ((op = s_and) and cond) or ((op = s_or) and not cond)
              then begin
                     lab := getlabel;
                     gencondjump (tree[x + t_op1], not cond, lab);
                     gencondjump (tree[x + t_op2], not cond, lab);
                     genref (i_br, target);
                     genlab (lab)
                   end
              else begin
                     gencondjump (tree[x + t_op1], cond, target);
                     gencondjump (tree[x + t_op2], cond, target)
                   end;
      s_eq  : begin
                if iszero (tree[x + t_op1])
                then texp (tree[x + t_op2])
                else if iszero (tree[x + t_op2])
                     then texp (tree[x + t_op1])
                     else texp2 (s_minus, tree[x + t_op1], tree[x + t_op2]);
                genjump (i_brz, cond, target)
              end;
      s_ls  : begin
                if iszero (tree[x + t_op2])
                then texp (tree[x + t_op1])
                else texp2 (s_minus, tree[x + t_op1], tree[x + t_op2]);
                genjump (i_brn, cond, target)
              end;
      else    begin
                texp (x);
                genjump (i_brz, not cond, target)
              end
    end
  end;

  procedure tbool (x:word; cond:boolean);
  var op, lab : word;
  begin
    op := tree [x + t_op];
    case op of
      s_not : tbool (tree[x + t_op1], not cond);
      s_and,
      s_or  : begin
                lab := getlabel;
                gencondjump (x, cond, lab);
                geni   (i_ldac, 0);
                geni   (i_br,   1);
                genlab (lab);
                geni   (i_ldac, 1)
              end;
      s_eq  : begin
                if iszero (tree[x + t_op1])
                then texp (tree[x + t_op2])
                else if iszero (tree[x + t_op2])
                     then texp (tree[x + t_op1])
                     else texp2 (s_minus, tree[x + t_op1], tree[x + t_op2]);

                if cond
                then begin
                       geni (i_brz,  2);
                       geni (i_ldac, 0);
                       geni (i_br,   1);
                       geni (i_ldac, 1)
                     end
                else begin
                       geni (i_brz,  1);
                       geni (i_ldac, 1)
                     end
              end;
      s_ls  : begin
                if iszero (tree [x + t_op2])
                then texp (tree[x + t_op1])
                else texp2 (s_minus, tree[x + t_op1], tree[x + t_op2]);

                if cond
                then begin
                       geni (i_brn,  2);
                       geni (i_ldac, 0);
                       geni (i_br,   1);
                       geni (i_ldac, 1)
                     end
                else begin
                       geni (i_brn,  2);
                       geni (i_ldac, 1);
                       geni (i_br,   1);
                       geni (i_ldac, 0)
                     end
              end;
      else    begin
                texp (x);
                if not cond
                then begin
                       geni (i_brz,  2);
                       geni (i_ldac, 0);
                       geni (i_br,   1);
                       geni (i_ldac, 1)
                     end
              end
    end
  end;

  procedure genstatement (x:word; seq:boolean; clab:word; tail:boolean);
  var
    op, op1 : word;
    lab, elselab : word;

    thenpart, elsepart : word;

    function funtail (tail:boolean) : boolean;
    begin
      funtail := infunc and tail
    end;

  begin
    op := tree [x + t_op];
    if op = s_semicolon
    then begin
           genstatement (tree [x + t_op1], true,    0, false);
           genstatement (tree [x + t_op2],  seq, clab,  tail)
         end
    else if (op = s_if) and (clab = 0)
         then begin
                lab := getlabel;
                genstatement (x, true, lab, tail);
                genlab (lab)
              end
         else if op = s_if
              then begin
                    thenpart := tree [x + t_op2];
                    elsepart := tree [x + t_op3];
                    if (not funtail (tail)) and ((tree [thenpart + t_op] = s_skip) or
                                                 (tree [elsepart + t_op] = s_skip))
                    then begin
                           gencondjump (tree[x + t_op1], tree[thenpart + t_op] = s_skip, clab);
                           if tree[thenpart + t_op] = s_skip
                           then genstatement (elsepart, seq, clab, tail)
                           else genstatement (thenpart, seq, clab, tail)
                         end
                    else begin
                           elselab := getlabel;
                           gencondjump  (tree[x + t_op1], false, elselab);
                           genstatement (thenpart, false, clab, tail);
                           genlab       (elselab);
                           genstatement (elsepart,   seq, clab, tail)
                         end
                   end
              else if funtail (tail)
                   then if op <> s_return
                        then generror  (ws('"return" expected'))
                        else begin
                               op1 := tree [x + t_op1];
                               if tree [op1 + t_op] = s_fncall
                               then tcall (op1, seq, clab, tail)
                               else begin
                                      texp  (tree [x + t_op1]);
                                      genbr (seq, clab)
                                    end
                             end
                   else if (op = s_while) and (clab = 0)
                        then begin
                               lab := getlabel;
                               genstatement (x, false, lab, false);
                               genlab (lab)
                             end
                        else if op = s_while
                             then begin
                                    lab := getlabel;
                                    genlab (lab);
                                    gencondjump  (tree [x + t_op1], false, clab);
                                    genstatement (tree [x + t_op2], false, lab, false)
                                  end
                             else if op = s_pcall
                                  then tcall (x, seq, clab, tail)
                                  else if op = s_stop
                                       then begin
                                              geni (i_ldac, 0);
                                              geni (i_opr, o_svc)
                                            end
                                       else begin
                                              if op <> s_skip
                                              then if op = s_ass
                                                    then genassign (tree[x + t_op1], tree[x + t_op2])
                                                    else if op = s_return
                                                         then generror (ws('misplaced "return"'));
                                              genbr (seq, clab)
                                            end
  end;



  procedure genprocs (x:word);
  var body, savetreep, pn : word;

    procedure optimise (x:word);
    var op : word;
    begin
      op := tree[x + t_op];
      case op of
        s_return    : tree [x + t_op1] := optimiseexpr (tree [x + t_op1]);
        s_ass,
        s_pcall     : begin
                        tree [x + t_op2] := optimiseexpr (tree [x + t_op2]);
                        tree [x + t_op1] := optimiseexpr (tree [x + t_op1])
                      end;
        s_while     : begin
                        tree [x + t_op1] := optimiseexpr (tree [x + t_op1]);
                        optimise (tree[x + t_op2])
                      end;
        s_if        : begin
                        tree [x + t_op1] := optimiseexpr (tree [x + t_op1]);
                        optimise (tree [x + t_op2]);
                        optimise (tree [x + t_op3])
                      end;
        s_semicolon : begin
                        optimise (tree [x + t_op1]);
                        optimise (tree [x + t_op2])
                      end;
        else ;
      end
    end;

  begin
    if tree [x + t_op] = s_semicolon
    then begin
           genprocs (tree [x + t_op1]);
           genprocs (tree [x + t_op2])
         end
    else begin
           savetreep := treep;

           namep := nameb;
           pn := findname (tree [x + t_op1]);
           proclabel := names_v [pn];
           procdef   := names_d [pn];

           infunc := tree [procdef + t_op] = s_func;
           body := tree [x + t_op3];
           if infunc
           then stk_init (2)
           else stk_init (1);

           declformals (tree[x + t_op2]);
           genlab (proclabel);
           genentry;
           stk_init (1);
           decllocals (tree [body + t_op1]);
           setstack;

           optimise     (tree [body + t_op2]);
           genstatement (tree [body + t_op2], true, 0, true);
           genexit;

           treep := savetreep
         end
  end;



  procedure flushbuffer;
  var
    bufferp, last   : word;
    offset, stksize : word;
    flag, loadstart : word;

    function instlength (opd : word) : word; // !!! generalize to bytesperword
    var v, n : word;
    begin
      if (opd >= 0) and (opd < 16)
      then n := 1
      else begin
             n := mul2 (bytesperword,2);
             if opd < 0
             then begin
                    v := mul2 (divf (opd, 256), 256); // clear lsb byte ?
                    while divf (v, word ($10000000)) = $f do // shr 32-4 bits
                    begin
                      v := mul2 (v, 16); // shr 4 bits
                      n := n - 1
                    end
                  end
             else begin
                    v := opd;
                    while divf (v, word ($10000000)) = 0 do // shr 32-4 bits
                    begin
                      v := mul2 (v, 16); /// shr 4 bits
                      n := n - 1
                    end
                  end
           end;
      instlength := n
    end;

    // !!! p not used bufferp passed
    function cb_reflength (p:word) : word;
    var ilen, labaddr : word;
    begin
      ilen := 1;
      labaddr := labval [cbv_low];
      while ilen < instlength (labaddr - (cb_loadpoint + ilen)) do
        ilen := ilen + 1;
      cb_reflength := ilen
    end;

    // !!! p not used in reflength, bufferp passed
    function cb_laboffset (p:word) : word;
    begin
      cb_laboffset := labval [cbv_low] - (cb_loadpoint + cb_reflength (p))
    end;

    // !!! p not used bufferp passed
    function cb_stackoffset (p, stksize : word) : word;
    var offs : word;
    begin
      offs := cbv_low;
      if (offs - pflag) < 0
      then cb_stackoffset := stksize - offs
      else cb_stackoffset := stksize + (offs - pflag)
    end;

    procedure cb_unpack (p : word);
    var x : word;
    begin
      x := codebuffer [p];
      cbv_flag := divf (x, cb_flag);

      x := rem (x, cb_flag);
      cbv_high := divf (x, cb_high);

      x := rem (x, cb_high) - 65536;
      cbv_low := x
    end;



    procedure expand;
    var bufferp, offset, stksize, flag : word;
    begin
      bufferp := 0;
      while bufferp < cb_bufferp do
      begin
        cb_unpack (bufferp);
        flag := cbv_flag;
        case flag of
          cbf_entry  : begin
                         stksize := cbv_low;
                         cb_loadpoint := cb_loadpoint + instlength (-stksize) + 4
                       end;
          cbf_lab    : labval [cbv_low] := cb_loadpoint;
          cbf_inst   : cb_loadpoint := cb_loadpoint + instlength (cbv_low);
          cbf_bwdref : cb_loadpoint := cb_loadpoint + cb_reflength (bufferp);
          cbf_pexit  : cb_loadpoint := cb_loadpoint + instlength (stksize) + 5;
          cbf_fnexit : cb_loadpoint := cb_loadpoint + instlength (stksize) + instlength (stksize + 1) + 5;
          cbf_var    : cb_loadpoint := cb_loadpoint + 4;
          cbf_stack  : begin
                         offset := cb_stackoffset (bufferp, stksize);
                         cb_loadpoint := cb_loadpoint + instlength (offset)
                       end;
          cbf_const  : begin
                         offset := cbv_low + cb_conststart;
                         cb_loadpoint := cb_loadpoint + instlength (offset)
                       end;
          cbf_string : begin
                         offset := cbv_low + cb_stringstart;
                         cb_loadpoint := cb_loadpoint + instlength (offset)
                       end;
          cbf_fwdref : begin
                         offset := cb_laboffset (bufferp);
                         if offset > 0
                         then cb_loadpoint := cb_loadpoint + cb_reflength (bufferp)
                         else cb_loadpoint := cb_loadpoint + 1
                       end;
          cbf_constp : begin
                         cb_conststart := divf (cb_loadpoint, 4);
                         cb_stringstart := cb_conststart + constp;
                         cb_loadpoint := cb_loadpoint + mul2 (constp + stringp, 4)
                       end;
          else         cmperror (ws('code buffer error'));
        end;
        bufferp := bufferp + 1
      end
    end;

    procedure outbin (d : byte);
    begin
      selectoutput (binstream);
      putval (rem (d, 256));
      selectoutput (messagestream)
    end;

    procedure out1 (inst, opd : word);
    begin
      outbin (mul2 (inst, 16) + rem (opd, 16))
    end;

    procedure outinst (inst, opd : word); // !!! generalize to bytesperword
    var v, n : word;
    begin
      if (opd >= 0) and (opd < 16)
      then out1 (inst, opd)
      else begin
             n := 28;
             if opd < 0
             then begin
                    v := mul2 (divf (opd, 256), 256);
                    while divf (v, word ($10000000)) = $f do // exp2 (24)
                    begin
                      v := mul2 (v, 16);
                      n := n - 4
                    end;
                    out1 (i_nfix, divf (opd, exp2 (n)));
                    n := n - 4
                 end
            else begin
                   v := opd;
                   while divf (v, word ($10000000)) = 0 do // exp2 (24)
                   begin
                    v := mul2 (v, 16);
                    n := n - 4
                   end
                 end;
            while n > 0 do
            begin
              out1 (i_pfix, divf (opd, exp2 (n)));
              n := n - 4
            end;
            out1 (inst, opd)
          end
    end;

    procedure outword (w : word);  // !!! generalize to bytesperword
    begin
      outbin (w);
      outbin (divf (w,     $100)); // exp2 ( 8)
      outbin (divf (w,   $10000)); // exp2 (16)
      outbin (divf (w, $1000000))  // exp2 (24)
    end;

    procedure outhdr;              // !!! generalize to bytesperword
    var w, entrypoint, offset : word;
    begin
      w := divf (cb_loadpoint + 3, 4);
      entrypoint := labval [entrylab];
      outword (w);
      offset := entrypoint - 4;
      out1 (i_pfix, divf (offset, $1000));
      out1 (i_pfix, divf (offset,  $100));
      out1 (i_pfix, divf (offset,   $10));
      out1 (i_br, offset)
    end;

    procedure outconsts;
    var count : word;
    begin
      count := 0;
      while count < constp do
      begin
        outword (consts[count]);
        count := count + 1
      end
    end;

    procedure outstrings;
    var count : word;
    begin
      count := 0;
      while count < stringp do
      begin
        outword (strings[count]);
        count := count + 1
      end
    end;

    procedure outvar (d:word);
    begin
      outword (d)
    end;

  begin
    loadstart := mul2 (m_sp, 4);
    cb_loadpoint := loadstart;

    last := 0;
    expand;
    while cb_loadpoint <> last do
    begin
      last := cb_loadpoint;
      cb_loadpoint := loadstart;
      expand
    end;
    codesize := cb_loadpoint;

    outhdr;
    bufferp := 0;
    cb_loadpoint := loadstart;
    while bufferp < cb_bufferp do
    begin
      cb_unpack (bufferp);
      flag := cbv_flag;
      case flag of
        cbf_entry  : begin
                       stksize := cbv_low;
                       outinst (i_ldbm,     m_sp);
                       outinst (i_stai,        0);
                       outinst (i_ldac, -stksize);
                       outinst (i_opr,     o_add);
                       outinst (i_stam,     m_sp);
                       cb_loadpoint := cb_loadpoint + instlength (-stksize) + 4
                     end;
        cbf_inst   : begin
                       outinst (cbv_high, cbv_low);
                       cb_loadpoint := cb_loadpoint + instlength (cbv_low)
                     end;
        cbf_var    : begin
                       outvar (cbv_low);
                       cb_loadpoint := cb_loadpoint + 4
                     end;
        cbf_stack  : begin
                       offset := cb_stackoffset (bufferp, stksize);
                       outinst (cbv_high, offset);
                       cb_loadpoint := cb_loadpoint + instlength (offset)
                     end;
        cbf_string : begin
                       offset := cbv_low + cb_stringstart;
                       outinst (i_ldac, offset);
                       cb_loadpoint := cb_loadpoint + instlength (offset)
                     end;
        cbf_lab    : ;
        cbf_bwdref,
        cbf_fwdref : begin
                       offset := cb_laboffset (bufferp);
                       if cb_reflength (bufferp) > instlength (offset)
                       then outinst (i_pfix, 0);
                       outinst (cbv_high, offset);
                       cb_loadpoint := cb_loadpoint + cb_reflength (bufferp)
                     end;
        cbf_const  : begin
                       offset := cbv_low + cb_conststart;
                       if cbv_high = r_areg
                       then outinst (i_ldam, offset)
                       else outinst (i_ldbm, offset);
                       cb_loadpoint := cb_loadpoint + instlength (offset)
                     end;
        cbf_constp : begin
                       cb_conststart  := divf (cb_loadpoint, 4);
                       cb_stringstart := cb_conststart + constp;
                       cb_loadpoint   := cb_loadpoint + mul2 (constp + stringp, 4);
                       outconsts;
                       outstrings
                     end;
        cbf_pexit  : begin
                       outinst (i_ldbm,    m_sp);
                       outinst (i_ldac, stksize);
                       outinst (i_opr,    o_add);
                       outinst (i_stam,    m_sp);
                       outinst (i_ldbi, stksize);
                       outinst (i_opr,    o_brb);
                       cb_loadpoint := cb_loadpoint + instlength (stksize) + 5
                     end;
        cbf_fnexit : begin
                       outinst (i_ldbm,      m_sp);
                       outinst (i_stai, stksize+1);
                       outinst (i_ldac,   stksize);
                       outinst (i_opr,      o_add);
                       outinst (i_stam,      m_sp);
                       outinst (i_ldbi,   stksize);
                       outinst (i_opr,      o_brb);
                       cb_loadpoint := cb_loadpoint + instlength (stksize) + instlength (stksize + 1) + 5
                     end;
      end;
      bufferp := bufferp + 1
    end
  end;



  procedure translate (t:word);
  var mainlab, link : word;

    procedure initlabels;
    var l : word;
    begin
      l := 0;
      while l < labval_size do
      begin
        labval [l] := 0;
        l := l + 1
      end
    end;

    procedure initbuffer;
    begin
      cb_bufferp   := 0;
      cb_loadpoint := 0;

      constp  := 0;
      stringp := 0
    end;

  begin
    namep := 0; nameb := 0;

    initlabels;
    labelcount := 1;

    initbuffer;
    arrayspace := 0;
    stk_init (m_sp + 1);

    declglobals (tree [t + t_op1]);
    tglobals; //   !!! (tree [t + t_op1]);
    gen (cbf_constp, 0, 0);
    declprocs (tree [t + t_op2]);
    nameb    := namep;
    entrylab := getlabel;
    mainlab  := getlabel;
    link     := getlabel;

    genlab (entrylab);
    genref (i_ldap,    link);
    genref (i_br,   mainlab);
    genlab (link);
    geni   (i_ldac,       0); // shoud we use s_stop?
    geni   (i_opr,    o_svc);
    genlab (mainlab);

    genprocs (tree [t + t_op2]);
    flushbuffer
  end;



// !!! duplicate identifiers,
//     zero params procedures and functions,
//     actual parameter types,
//     true and false values,
//     bytesperword generalize

var t : word;
begin
  fname := 'xhex';
  if paramstr(1)<>''
  then fname := paramstr(1);

  init;

  selectoutput (messagestream);

  t := formtree;

  newline;
  prints (ws('tree size        : ')); printn (treep); // mul (treep ,bytesperword));
  prints (ws(' words')); newline;

  translate (t);

  prints (ws('program size     : ')); printn (codesize);
  prints (ws(' bytes')); newline;
  prints (ws('program + arrays : ')); printn (codesize + mul (arrayspace, bytesperword));
  prints (ws(' bytes')); newline;
  newline;

  done (0)
end.