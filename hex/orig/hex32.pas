// original translation of hex.c, only style differences, prep for generalisations
program hex;
type  word = longword; // 32 bit

const wordsize = 4;
      memsize  = 200000; // in words
      files    = 8;

type  instruction    = (ldam,ldbm,stam,  ldac,ldbc,ldap,  ldai,ldbi,stai,
                        br,brz,brn, none, opr,pfix,nfix);
      oprinstruction = (brb,add,sub,svc);

var   wmem : array [0..memsize-1]          of word;
      bmem : array [0..memsize*wordsize-1] of byte absolute wmem;

      f    : array [0..files-1] of file of byte;
      open : array [0..files-1] of boolean;



  procedure load;
  var
    f   : file of byte;
    len : word;

    i, lo : word;

    function in16 : word;
    var lo, hi : byte;
    begin
      read (f,lo,hi);
      in16 := hi * 256 + lo
    end;

  begin
    assign (f,'a.bin');
    reset  (f);

    lo  := in16;
    len := ((in16 shl 16) + lo) * wordsize;
    for i := 1 to len do
      read (f, bmem [i]);

    close (f)
  end;

  function fname (i:word) : string;
  begin
    fname := 'file' + chr (ord('0')+i)
  end;

  procedure simout (b,s : word);
  begin
    if s<256
    then write (chr (b mod 256))
    else begin
           s := (s mod 256) mod files;
           if not open [s]
           then rewrite (f[s]);
           write (f[s], b mod 256)
         end
  end;

  function simin (s : word) : word;
  var ch : char; b : byte;
  begin
    if s<256
    then read (simin)  // simin := ord (readkey)
    else begin
           s := (s mod 256) mod files;
           if not open [s]
           then reset (f[s]);
           read (f[s], b);
           simin := b
         end
  end;

var
  pc, sp, areg, breg, oreg : word;
  running, clear           : boolean;

  b : byte;
  i : word;

begin
  for i := 0 to files-1 do
  begin
    open [i] := false;
    assign (f[i], fname(i))
  end;

  load;

  oreg := 0;
  pc   := 0;

  running := true;
  while running do
  begin
    b    := bmem [pc];
    pc   := pc + 1;
    oreg := oreg + b mod 16;

    clear := true;
    case instruction (b div 16) of
      ldam : areg := wmem [oreg];
      ldbm : breg := wmem [oreg];
      stam : wmem [oreg] := areg;

      ldac : areg := oreg;
      ldbc : breg := oreg;
      ldap : areg := pc + oreg;

      ldai : areg := wmem [areg + oreg];
      ldbi : breg := wmem [breg + oreg];
      stai : wmem [breg + oreg] := areg;

      brn  : if areg > $80000000 then pc := pc + oreg;
      brz  : if areg = 0         then pc := pc + oreg;
      br   :                          pc := pc + oreg;

      pfix : begin oreg :=             oreg * 16; clear := false end;
      nfix : begin oreg := $ffffff00 + oreg * 16; clear := false end;

      opr : begin
              case oreg of
                ord (brb) : pc := breg;
                ord (add) : areg := areg + breg;
                ord (sub) : areg := areg - breg;
                ord (svc) : begin
                              sp := wmem [1];
                              case areg of
                                0 : running := false;
                                1 : simout (wmem [sp+2], wmem [sp+3]);
                                2 : wmem [sp+1] := simin (wmem [sp+2] mod 256)
                                else running := false
                              end
                            end;
                else  running := false
              end
            end
       else running := false
    end;

    if clear then oreg := 0
  end;

  for i := 0 to files-1 do
    if open [i]
    then close (f[i]);
end.