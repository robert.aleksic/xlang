// original translation of hex8.c, only style differences, prep for generalisations
program hex8;
type
  word   = byte;
  nibble = 0..15;

  instruction = (ldam,ldbm,stam,  ldac,ldbc,ldap,  ldai,ldbi,stai,
                 br,brz,brn, brb,add,sub, pfix);

var
  mem : array [word] of byte;

  procedure load;
  const eot = #0;
  var
    f  : file of char;
    ch : char;
    p  : word;

    procedure getch;
    begin
      if eof (f)
      then ch := eot
      else read (f,ch)
    end;

    procedure getbyte (var b:byte);

      function hex : nibble;
      const valid = ['0'..'9','a'..'f','A'..'F', eot];
      begin
        while not (ch in valid) do getch;
        case ch of
          '0'..'9' : hex := ord (ch) - ord ('0');
          'a'..'f' : hex := ord (ch) - ord ('a');
          'A'..'F' : hex := ord (ch) - ord ('A');
          eot      : hex := 0
        end
      end;

    begin
      b := hex;
      b := b * 4 + hex
    end;

  begin
    assign (f,'a.bin');
    reset  (f);

    getch;
    p := 0;
    while ch <> eot do
    begin
      getbyte (mem[p]);
      p := p+1
    end;
    close (f)
  end;

var
  pc, areg, breg, oreg : word;
  running, clear       : boolean;

  b  : byte;

begin
  load;

  oreg := 0;
  pc   := 0;

  running := true;
  while running do
  begin
    b    := mem [pc];
    pc   := pc + 1;
    oreg := oreg + b mod 16;

    clear := true;
    case instruction (b div 16) of
      ldam : areg := mem [oreg];
      ldbm : breg := mem [oreg];
      stam : mem[oreg] := areg;

      ldac : areg := oreg;
      ldbc : breg := oreg;
      ldap : areg := pc + oreg;

      ldai : areg := mem [areg + oreg];
      ldbi : breg := mem [breg + oreg];
      stai : mem [breg + oreg] := areg;

      brb  : pc := breg;
      brz  : if areg =   0 then pc := pc + oreg;
      brn  : if areg > 127 then pc := pc + oreg;
      br   : if oreg <> $fe
             then pc := pc + oreg
             else running := false;

      add  : areg := areg + breg;
      sub  : areg := areg - breg;

      pfix : begin oreg := oreg * 16; clear := false end
    end;

    if clear then oreg := 0
  end;

  writeln ('areg = ',areg)
end.