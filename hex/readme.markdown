Introduction
============

Hex is small and simple micropocessor designed by prof. David May at Bristol university for teaching computer science. Memory is treated either as array of words (for data access) or bytes (for instruction access). For hex8, word is 1 byte. It have 2 registers, `areg` and `breg`. It's instructions are one byte, regardles of microprocesor word length. Instructions might have one byte operand, stored in `oreg`. If operand is bigger then 4 bits, it can be extended with `pfix` and `nfix` instructions. On startup, program counter `pc` is set to zero, and instructions are fetched byte by byte. Data addresses are aligned on processor word length.

Instruction set is as follows:

    * ldac, ldbc - constant to register (areg|breg := oreg)
    * ldam, ldbm - register from memory (areg|breg := mem[oreg])
    * ldai, ldbi - register indirect from memory (areg|breg := mem[areg|breg+oreg])
    * ldap       - load code pointer to areg (areg := mem[pc+oreg])
    * stam       - store areg to constant address (mem[oreg]      := areg)
    * stai       - store areg to constant offset  (mem[breg+oreg] := breg)
    * br         - branch relative (pc := pc+oreg)
    * brz        - branch relative if areg = 0
    * brn        - branch relative if areg < 0
    * pfix       - prefix (oreg := prev_oreg<<4  || oreg)
    * nfix       - prefix (oreg := ~prev_oreg<<4 || oreg)
    * opr        - extended operatorless instructions encoded in oreg
    * svc        - service call

Extended instructions are also one byte:

    * brb - opr 0 - branch absolute on breg (pc := breg)
    * add - opr 1 - areg := (areg + breg) mod (maxword+1)
    * sub - opr 2 - areg := (areg - breg) mod (maxword+1)

Service calls as I implemented them currently:

    * stop - svc 0
    * get  - svc 1 - wait for keypress and return it's code in areg
    * put  - svc 2 - write chr (areg) on screen

Pfix and nfix are intended to be automaticaly inserted by assembler.

This is small toolset writen in free pascal to support work with hex8 assembler.

It contains:

  * asm8 assembler which translate from assembler to hex8 (.asm8 -> .hex8)
  * hex8 interpreter which is used to interpret hex8 files (.hex8)
  * sublime text syntax highling and build system



Instalation
===========

To compile interpreter and assembler and clean extra files

    fpc bin/hex8
    fpc bin/asm8
    delp bin

On linux/mac you can also create symbolic links in /usr/local/bin

    sudo ln -s /home/username/hex/bin/hex8 /usr/local/bin/hex8
    sudo ln -s /home/username/hex/bin/asm8 /usr/local/bin/asm8

so that you can invoke them as `hex8` and `asm8` instead of `bin/hex8` and `bin/asm`


If you want sublime build system, syntax highlighting and colour scheme, copy content of sublime folder to Packages folder of sublime text editor. It will also install free pascal build system.



Differencies
============
  * `nfix`, `opr` and `svc` are introduced on hex8 for future compatibility with hex16/32/64.
  * `svc` is encoded as #c (left unused on hex) to give you `areg` for svc parameters
  * `pfix` and `nfix` are #e and #f like in hex
  * `opr 0` is `brb`, `opr 1` is `add` and `opr 2` is `sub`
  * `svc 0` is `stop`, `svc 1` is `get`, `svc 2` is `put`
  * all svc's and opr's with areg out of range are treated as `stop`
  * `br #fe` - dead loop is also treated as `stop` (it is same as in hex8 document, but will be same for other hex processors)



Interpreter
===========

Interpreter can be run with 'hex8 fname', 'hex8 fname -d' for debuger or 'hex8 fname -t' for timing run time (if it does not have `put's` or `get's`).
In debuger you can use `q` to quit, `x` to switch between hexadecimal and decimal output, `m` to show memory, `c` to continue to the end or just hit `enter` to execute current instruction.



Assembler
=========

You can call assembler with `asm fname -v` for verbose output (fname.hex8 file will include detailed translation with source asm8 file).

Comment start with `--` or `|` and ends on end of line.
`;` is used to separate items within line.

Assembler includes constants, variables and labels (see examples).

Character constants (`'ch'`) and hex contants (`#xx`) can be used interchangable with numeric constants. Numeric constants can be signed and unsigned. (On hex8, range is -128..255). Constant `bpw` is defined to be 1 for hex8 (and will be 2 for hex16, 4 for hex32 and 8 for hex64).

Variables can be initialized with `init`. Array literals are specified by enclosing elements in `[` and `]`, and separated by `,`. Array elements can be repeated using `times` reserved word. For each initialized array constant `array_name.len` is defined automatically after variable definition.

Strings (delimited by `"`) are treated as array constants. Following sequences can be used instead of characters in charcter literals or strings - `\\`, `\n` - new line, `\t` - tab, `\0` - byte 0, `\'` - aphostrophe, `"` - double apostrophe.

Extended mnemonics can be used instead of `opr's` and `svc's` (brb, add, sub, stop, put and get). Basic type checking is done so that labels can be used only for branches and `ldap`, and only vars/constants can be used in data instructions. This may further be strenghtened in the future.

Everything in file after . in `end.` is treated as comment.

*****************************************************************************************
*****************************************************************************************
*****************************************************************************************

open hex questions
==================
* hex8 differs from hex by excluding opr instruction and using 'br #fe as stop which makes sense since 'br #fe' is actually dead loop, thus processor do not have undefined behavior (except 'br #ff' although it is similar to 'br #f')
* hex includes 'svc', stop is 'ldac 0;svc',
* in hex, instruction #c is undefined


implementation
==============

I have decided to implement hex8 similary to hex with following differences:
  - hex8 also have opr and svc instruction implemented as in hex
  - svc instruction is #c
  - svc instruction have one operand - service call number
  - stop is either 'svc 0', 'svc >maxsvc', 'opr >maxopr' or 'br #fe'
  - nfix is same as pfix

  - svc 1 - put - write areg to screen
  - svc 2 - get - read areg from keyboard

Comment start with | or -- and ends at the end of line.
Label is identifier followed by :. It can be data label or code label. If label is at the end of file it is data label.
Number is either 'char' #hex or [-]{1,digits}
String is "characters" and is represented by length and then string bytes.
Data block is one or more numbers or strings (which can be prefixed by [x] to repeat them x times)
Instruction can be either specified by basic or extended mnemonic.
If instruction have argument it can be number or code label.
Multiple labels can point to same location.
Instructions or data blocks can be followed by ; and then label, datablock or instruction.
Pfix and nfix instructions are inserted by assembler.

Hex8 interpreter can be used as profiler. Program will be timed, but puts and gets will be excluded.

*****************************************************************************************

memory usage
=============

- processor can be 8,16,32 or 64 bits
- constant wordlen is set in assembler to 1,2,4 or 8 depending on processor
- code for hex8 start with br after vars.
- memmory starts at 0, where usually br xx is stored, word addres 1 is used as stack pointer and hex interpreter initializes it to MaxMem (255 for 8-bits, otherwise it's implementation dependend)
- maybe better implementation will be to use registers as stack (as in transputer) and include dup/exchg
- with something like workspace pointer on transputer, instruction set can be simplified
    (in line with: ldc, ldl, ldg, stl, stg, setwp, j, jz, jeq, dup, exchg, add, sub, pfix, nfix, opr)

8 bit processor:  0: br f,  1: sp ff, 2..15: global vars, 16: br f/code, 31: global vars, br f/code
16 bit processr:  w0: br ff, w1: sp ffff, w2: br


ideas for better assembler
==========================


    -- ; is used only to delimit items in a line
    -- types byte and word are dependant on processor width, registers are words, labels are word addresses
    -- all expressions are 64 bits? and then truncated to word (signed or unsigned)

    -- constant expression includes +,-,*,/,pow,and,or,not,xor,shl,shr,log2,log10 (not 0 = -1; and = * not 0; or )

    --
    -- predefined
    --
    --   const
    --     true  = sword (-1)
    --     false = 0
    --     arch   = 1               -- 0,  1,  2 or  3
    --     maxnum = sword (-1)      -- #ff...
    --     minsig = 1 shl (bipw-1)  -- #80...
    --     maxsig = minsig-1        -- #7f...
    --
    --     bypw   = w ** arch -- 1,  2,  4 or  8
    --     bipw   = 8 * bypw  -- 8, 16, 32 ir 64
    --

    alias
      r0 is areg
      r1 is breg

    set
      strict = true -- all instructions must be from extended set


    --------

    lda c - ldac, ldbc
    lda v - ldam, ldbm

    add - a := a+b
    sub - a := a-b

    br   rel         - while, call -- br
    j    b           - call        -- ldb; brb
    brc  0/+, rel    - if          -- bn, bz

    stop -- br #fe
    skip -- pfix 0 or nothing

    ------

    ld r, o      - ldac; ldbc
    ld r, [o]    - ldam; ldbm
    ld r, [r+o]  - ldai; ldbi

    ld [o],   a      - stam c
    ld [b+o], a      - stai c
    ld a,     label  - ldap l


    ld a,b   -- ldac 0; add
    ld a,-b  -- ldac 0; sub

    -----

    a   := 3,[3],[a+3], label
    b   := 3,[3],[b+3]
    [3] := a|b

    ld r,     o      - ldac c; ldbc c
    ld r,     [o]    - ldam c; ldbm c
    ld a,     [b+o]  - ldai c
    ld a,     label  - ldap l
    ld [o],   a      - stam c
    ld [b+o], a      - stai c

    -----

    --
    -- negcond can be only a>=0 or a<>0 nn|nz
    -- cond can be only a<0 or a=0       n|z
    --

    if negcond  -- brc end
    end         -- end:

    if cond -- brc then; br end; then:
    end     -- end:

    if negcond -- brc else
    else       -- br end; else:
    end        -- end:

    if cond -- brc then; br else; then:
    else    -- br end; else:
    end     -- end:

    repeat        -- rep:
    until cond    -- brc end; br rep; end:

    repeat        -- rep:
    until negcond -- brc rep

    test           -- while:
      -- calc a
    while cond     -- brc end:
      -- while content
    end           -- br test

    if cond        -- brc else
    else           -- br end; else:
      repeat          -- rep:
      until notcond   -- brc rep
    end              end:

    -- max 15 cases -- a in range 0..127 -- b is destroyed
    case a of  -- brn else; ldbc n; sub; brn cont: br else; add; brz c0; ldbc 1;  sub; brz c1; sub; brz c2...; br else
      0 :      -- c0: ldac 0; ... br end:
      1 :      -- c1: ldac 1; ... br end:
      n :      -- cn: ldac n; ... br end:
      else     -- else:
    end        -- end:

    if a in [0..n] --
      case
        0:
        1:
        n:
      end
    else

    for i in a to b  -- ldac a; stam i; for: ldbm b; sub; ldbc 1; sub; brz end
    end              -- ldam i; ldbc 1; add; stam i; bra for

    for i in b downto a -- ldac b; stam i; for: ldbm a; sub; ldbc 1; add; brz end
    end                 -- ldam i; ldbc 1; sub; stam i; bra for


attempt at clearing syntax
===========================

    types
      int64 = signed 8 bytes
      int32 = signed 4 bytes
      int16 = signed 2 bytes
      int8  = -128..127

      uns64 = unsigned 8 bytes
      uns32 = unsigned 4 bytes
      uns16 = unsigned 2 bytes
      uns8  = unsigned byte

      byte  = 0..255 | -128..127
      boolean = true | false    nonzero | zero

      word = int wortd size;

    arrays
      conformantarray = []type         -- len, len * elements (0..len+1)
      fixedarray      = [constexp]type -- len * elements (0..len)
    record
      record mika:byte; laza:byte end -- mika = 0, laza = 1

    letter = 'a'..'z', 'A'..'Z'
    digit  = '0'..'9'
    hexdigit = '0'..'9', 'a'..'f', 'A'..'F'
    underscore = '_'

    idchar = letter | digit | underscore
    id = letter [0 idchar]

    number = #[1 hexdigits] | 'char' | {-}digits

    mnemonic = ldam | ldbm | stam |
               ldac | ldbc | ldap |
               ldai | ldbi | stai |
               br   | brz  |  brn |
               svc  | opr
               brb  | add  |  sub     -- no pfix/nfix it should be automatically inserted

    reservedwords = mnemonics | const | var

    constblock = const {1; constdecl}
    constdecl  = id = constexpr

    varblock = var {1; vardecl}
    vardecl  = id : {[constexp]} type = constexpr

    element = {unaryop} (expression) | literal


    strucutre:
      line: {{label: }+ {command | bytes}+ }  {[!|--] comment} eol


further ideas
==============

    -- compare two numbers [0..127] by subtraction
    hex8 compare

    const
      parnum = 3

    var
      x init 1
      y init 120
      z

      -- comp two const at stack, return neg,0 or pos
      proc comp (const v1, v2; var v3) --
      begin
        lda v1; ldb v2 -- sp[1]; sp[2]
        sub
        sta v3 -- sp[3]
      end

    begin
      alloc parnum+1
      lda x;  sta sp[0]
      lda y;  sta sp[1]
      lda @z; sta sp[2]
      call comp
      alloc -parnum-1
    end.


    @sp : 255

    x : -1
    y : -128

    comp:
      ldam sp; ldai 1 -- first val parameter
      ldbm sp; ldbi 2 -- second val parameter

      sub
      brn itsb
    itisa:
      br done:
    itisb:
      ldac 0
    done:
      add

    return:
      ldbm sp; ldbi 3 -- return address
      brb

    start:
      -- ldac $ff - init stack pointer
      -- stam sp

      ldbm sp; ldac -3; add; stam sp;   -- aloc 3;
      ldam x; stai 1                    -- first val parameter
      ldam y; stai 2                    -- second val parameter
      ldap after_call; stai 3           -- push ret_addr
      br comp;
    after_call:
      stam x
      ldbm sp; ldac 3; add; stam sp    -- dsps 3

      ldam x

    end:
      br $fe

X syntax change
=================

    declare
      (0, put, get)

      instream = 0; messagestream = 0; binstream = #100;
      eof = 255;

      -- tree node field selectors
      prefix t (op, op1, op2, op3);


      -- symbols
      prefix s (null, name, number, lbracket, rbracket, skip, lparen, rparen,
                fncall, pcall, if, then, else, while, do, ass, skip, begin, end,
                semicolon, comma, var, array, body, proc, func, is, stop, *4
                not, neg, val, string, skip 5,
                true, false, return,
                endfile is 60, diadic is 64,
                plus, minus, or, end, skip 3,
                eq, ne, ls, le, gr, ge, sub);

      -- up instruction codes
      prefix i (ldam, ldbm, stam, ldac, ldbc, ldap, ldai, ldbi, stai,
                br, brz, brn, skip, opr, pfix, nfix);
      prefix o (brb, add, sub, svc);

      (r_areg, r_breg);
      m_sp = 1;
      bytesperword = 4;

      -- lexical analyser
      nil = 0
      var outstream;

      nametablesize = 101;
      array nametable [nametablesize];

      treemax = 2000;
      array tree[tremax];
      var treep, namenode, nullnode, zeronode, numval, symbol;

      array wordv[100];
      var wordp, wordsize;

      array charv[100];
      var charp, ch;

      linemax = 200;
      array linev[linemax];
      vat linep, linelength, linecount;

      -- name scoping stack
      array names_d[500], names_v[500]
      var namep, nameb;
      pflag = #1000;

      var arrayspace

===========================


|
| x is constant
|   lda x = ldac x -- a or b
|
| global:
|
| x is global var, constant offset from 0
|   lda x = ldam x -- a or b
|   sta x = stam x -- a or b
|
| x global array fixed alloc x is constant offset from 0; constant offset
|   lda x[c] = ldam x+c -- a or b
|   sta x[c] = stam x+c -- a or b
|
| x global array fixed alloc x is constant offset from 0; var offset i
|   lda x[i] = ldam i; ldai x -- a or b
|   sta x[i] = ldbm i; stai x -- b destroyed
|
| x global pointer -- sp for example
|   lda x = ldam x; ldai 0  -- a or b
|   sta x = ldbc x; stai 0  -- b destroyed
|
| sp is global pointer
|   new n = ldbc n; ldam sp; sub; stam sp -- a and b destroyed
|   dsp n = ldbc n; ldam sp; add; stam sp -- a and b destroyed
|
| local (sp relative)
|
| local var x, stack relative; constant offset x
|   lda sp[x] = ldam sp; ldai 0; ldai x -- a or b
|   sta sp[x] = ldbm sp; ldbi 0; stai x -- b destroyed
|
| local array x, stack relative; constant offset c
|   lda x[c] = ldac sp; ldai 0; ldai x+c -- a or b
|   sta x[c] = ldbm sp; ldbi 0; stai x+c -- b destroyed
|
| local array x, stack relative; offset i
|   lda x[i] = ldam sp; ldai 0; ldbm i; add; ldai x -- b destroyed
|   sta x[i] = stam temp+0; ldam sp; ldai 0; ldbm i; add; stam temp+1; ldbm temp+1; ldam temp+0; stai 0
|

*****************************************************************************************
*****************************************************************************************
*****************************************************************************************

allocation can be - auto, after code, fixed address, aliased with other location, on stack (for locals)

types can be
  byte/char
  word (number of bytes depending on processor)
  enumerated (values are constants starting from zero, up to byte)
  arrays (starting from zero) with fixed len (array name.len is constant)
  structs (field adresses are constant offsets)

constants can be allocated, they are treated as constants, they can be read with `ldam`, but cannot with writen with `stam` and simmilar
constant expression are computed during compilation

variables have types, they can be global or local, initialized or not

  varname : type


