{$mode objfpc}
unit base;

{default parameters in ext; default integer is int32}

interface

uses
  sysutils, dateutils;

const
  eot = chr (26);
  eol = chr (13);
  tab = chr (9);

type
  int8  = shortint;
  int16 = smallint;
  int32 = longint;
  {int64 already exists}

  integer = int32;
  bigint  = int64;

  word8  = byte;
  word16 = word;
  word32 = longword;
  word64 = qword;

  bigword = word64;

  bignum = record
             case neg:boolean of
               true  : (i:int64);
               false : (w:word64)
           end;

  procedure skip;
  procedure error (s:string);

  function ext  (s:string; len:integer; ch : char = ' ') : string;
  function zext (s:string; len:integer) : string;

  function strfromch  (ch:char)    : string;
  function hexch      (b:byte)     : char;
  function hexstr     (w:bigword)  : string;
  function strfromnum (num:bigint) : string;

const
  millisecinday = 24*60*60*1000;

  function millisec : word64;
  function millisecdist (first,last:word64) : word64;
  function milisectostr (m:real) : string;



implementation

  procedure skip;
  begin
  end;

  procedure error (s:string);
  begin
    writeln ('error: ',s);
    halt (1)
  end;

  function ext (s:string; len:integer; ch:char = ' ') : string;
  var i : integer;
  begin
    if length (s) < len
    then for i := length (s) + 1 to len do s := ch+s;
    ext := s
  end;

  function zext (s:string; len:integer) : string;
  begin
    zext := ext (s,len,'0')
  end;

  function strfromch (ch:char) : string;
  var s : string;
  begin
    case ch of
      chr(33)..chr(126) : s := ch;
      ' '  : s := 'space';
      eot  : s := 'end of text';
      eol  : s := 'end of line';
      else  begin str (ord(ch),s); s := '('+s+')' end
    end;
    strfromch := s
  end;

  function hexch (b:byte) : char;
  begin
    case b of
      0..9 : hexch := chr(ord('0')+b)
      else   hexch := chr(ord('a')+b-10)
    end
  end;

  function hexstr (w:bigword) : string;
  var s : string;
  begin
    s := '';
    while w > 0 do
    begin
      s := hexch (w and $f)+s;
      w := w shr 4
    end;
    hexstr := s
  end;

  function strfromnum (num:bigint) : string;
  begin
    str (num,strfromnum)
  end;



  function millisec : word64;
  var
    d : tdatetime;
  begin
    d := now;
    millisec := (((hourof(d)*60)+minuteof(d))*60+secondof(d))*1000+millisecondof(d)
  end;

  function millisecdist (first,last:word64) : word64;
  begin
    if last >= first
    then millisecdist := last - first
    else millisecdist := last + (millisecinday - first)
  end;

  function milisectostr (m:real) : string;
  const pfix : array [1..3] of string[5] = ('mili','micro','nano');
  var i : integer; s : string;
  begin
    i := 1;
    while (m<1) and (i<3) do
    begin
     i := i+1;
     m := m * 1e3
    end;
    str (m:10:3, s);
    s := trim (s);
    milisectostr := s+' '+pfix[i]+'seconds'
  end;

begin

end.