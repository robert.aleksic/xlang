program hex8;

uses
  sysutils, crt, base, hexbase;

var
  timed   : boolean;
  debug   : boolean;
  showhex : boolean;

  noio : boolean;

  
  function numtostr (b:byte) : string;
  begin      
    if showhex 
    then numtostr := zext ( hexstr     (b), 2)
    else numtostr :=  ext ( strfromnum (b), 3)
  end;

  procedure showregs;
  var m : mnemonic;
  begin 
    m := extmnem; 
    write (numtostr(pc-1),': ',mnems[m], ' ':5-length(mnems[m]));    
    if ord(m) < 16
    then write (numtostr(oreg))
    else write (' ':3-ord(showhex));
    write (' [',numtostr(areg),' ',numtostr(breg), '] ')
  end;

  procedure showmemory;
  const num = 16;
  var i : integer;
  begin
    for i := 0 to 255 do
    begin
      if (i mod num) = 0 then write ('  ',numtostr(i),': ');
      write (numtostr(mem[i]),' ');
      if (i mod num) = num-1 then writeln
    end
  end;

  procedure load;
  var
    f   : text;
    fn  : string;
    ch  : char;
    i,b : byte;

    procedure getch;
    begin
      if eof (f) or (ch = eot)
      then ch := eot
      else if eoln (f)
           then begin ch := eol; readln (f) end
           else begin
                  read (f,ch);
                  case ch of
                    '.' : ch := eot;
                    commentch : begin
                                  readln (f);
                                  getch                     
                                end; 
                    else skip
                  end
                end
    end;

    procedure skipspaces; begin while ch in [' ',eol,tab] do getch end;

    procedure getbyte;
    var h : byte;
      procedure gethex;
      begin
        case ch of
          'A'..'F' : h := ord(ch)-ord('A')+10;
          'a'..'f' : h := ord(ch)-ord('a')+10; 
          '0'..'9' : h := ord(ch)-ord('0');
          else       error (strfromch(ch)+' - hex digit expected')
        end;
        getch
      end;
    begin
      gethex; b := h << 4; gethex; b := b + h; skipspaces
    end;

  begin
    debug   := false;
    timed   := false;
    showhex := true;

    case paramcount of
      0 : error ('name of '+hexext+' file expected');
      1 : skip;
      2 : if paramstr(2) = '-d'
          then debug := true
          else if paramstr(2) = '-t'
               then timed := true
               else error ('second parameter can be only "-d" to enable debuger or "-t" for timed run')
      else error ('command syntax is: '+hexext+' fname [-d|-t]')
    end;

    fn := paramstr(1);
    if fn[length(fn)]<>'.' then fn := fn+'.';
    fn := fn+hexext;
  
    if not fileexists (fn)
    then error ('File '+fn+' don''t exists');

    assign (f,fn);
    reset (f);
    getch;
    skipspaces;
    i := 0;
    while ch <> eot do
    begin
      getbyte;
      mem[i] := b;
      i := i+1
    end;
    close (f)
  end;

  procedure dostop;
    function u (b:byte):string;
    var s1,s2 : string[9];
    begin
      str (b,s1);
      if b > $7f
      then begin
            str ($ff-b+1,s2);
            s1 := s1+'(-'+s2+')'
           end;
      u := s1
    end;
    function u2 (b1,b2:byte):string;
    var w     : word;
        s1,s2 : string[13];
    begin
      w := (b1 << 8) or b2;
      str (w,s1);
      if w > $7fff
      then begin
             str ($ffff-w+1,s2);
             s1 := s1+'(-'+s2+')'         
           end;
      u2 := s1
    end;
  begin
    if not timed
    then if debug or noio
         then begin
                writeln ('memory: '); showmemory; 
                writeln ('registers:'); writeln ('  areg = ',u(areg),' breg = ',u(breg),' pc = ',pc, ' abreg = ',u2(areg,breg));
              end;
    running := false 
  end;

  procedure prompt;
  var s : string;
  begin
    showregs;
    readln (s);
    if s = 'c' then debug := false;
    if s = 'q' then dostop;
    if s = 'm'
    then begin
           showmemory;
           prompt      
         end;
    if s = 'x'
    then begin
           showhex := not showhex;
           prompt
         end
  end;

  procedure onerun;
  begin

    noio := true;

    areg := 0;
    breg := 0;


    running := true; pc := 0; oreg := 0;
    while running do 
    begin
      
      inst := mem[pc];
      pc   := pc + 1;      

      oreg := oreg or (inst and $0f);
      inst := inst >> 4;
      
      if debug and not (mnemonic(inst) in [pfix,nfix]) then prompt;
      if extmnem in [put,get,putn]
      then begin
             noio := false; 
             if timed
             then begin
                    inst := ord(br);
                    oreg := 0;
                  end
           end;
      
      case mnemonic (inst) of     
        {0} ldam : areg := mem [oreg];
        {1} ldbm : breg := mem [oreg]; 
        {2} stam : mem[oreg] := areg; 
        {3} ldac : areg := oreg;
        {4} ldbc : breg := oreg;
        {5} ldap : areg := pc + oreg;
        {6} ldai : areg := mem [areg + oreg];
        {7} ldbi : breg := mem [breg + oreg];
        {8} stai : mem [breg + oreg] := areg;
        {9} br   : if oreg = $fe 
                   then dostop 
                   else pc := pc + oreg;
        {a} brz  : if areg = 0   then pc := pc+oreg;
        {b} brn  : if areg > 127 then pc := pc+oreg;

        {e} pfix : oreg := oreg << 4;
        {f} nfix : oreg := oreg << 4;

        {d} opr  : case extmnem of
                     {0} brb  : pc := breg;
                     {1} add  : areg := areg + breg;
                     {2} sub  : areg := areg - breg;
                     {x} else   dostop
                    end;

        {c} svc  : case extmnem of
                     {0} stop : dostop;
                     {1} get  : areg := ord(readkey); {sort out ctrl c}
                     {2} put  : if chr(areg)=eol then writeln else write (chr(areg));
                     {2} putn : if breg=0 
                                then write (zext(strfromnum(areg),3)) 
                                else if areg>127
                                     then write ('-',zext(strfromnum(256-areg),3))
                                     else write ('+',zext(strfromnum(areg),3));
                     {x} else   dostop
                   end
      end;

      if not (mnemonic(inst) in [pfix,nfix])
      then oreg := 0

    end
    
  end;

  procedure timedrun;
  const 
    mintime   = 1500;
    calibruns = 100000;
  var
    backupmem : array [0..memsize-1] of byte;
  
    t, nrofruns   : word64;
    offt, runtime : double;


    function timefornruns (n:word64; run:boolean) : word64;
    var
      i   : integer;
      s,e : word64;
    begin
      s := millisec;
      for i := 1 to n do
      begin
        mem := backupmem;
        if run then onerun
      end;
      e := millisec;
      timefornruns := millisecdist (s,e);
    end;

  begin
    backupmem := mem;

    t := timefornruns (calibruns,false);
    offt := (t*1.0)/calibruns;

    nrofruns := 1;
    repeat
      writeln ('Nr of runs = ',nrofruns);
      t := timefornruns (nrofruns, true);
      if t < mintime
      then nrofruns := nrofruns * 5
    until t >= mintime;

    runtime := (t*1.0)/nrofruns-offt; 
    writeln ('Done, running time = ',milisectostr(runtime))
  end;

begin

  load;

  if not timed
  then onerun
  else timedrun

end.