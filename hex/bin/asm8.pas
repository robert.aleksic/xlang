program assembler;

uses
  sysutils, base, hexbase;

const
  buffersize = 1000000;

  maxconstants    = 100;
  maxvars         = 200;
  maxlabellen     =  20;
  maxdigitsinnum  =  10;

  maxcommands  = 10000;

  maxdatamultiplier = 100;
  maxbytes          = 100;


var
  chbuff    : array [0..buffersize-1] of char;
  chbufflen : integer;
  datapos   : integer;

type
  commrec = record
              lab      : string[maxlabellen];

              inst : integer;
              oreg : data;

              oplabel    : string[maxlabellen];
              oplabelref : integer;
              oplabelpos : integer;

              lineref : integer;
              bytepos : data;

              hasdata  : boolean;
              bytesnum : data;
              bytes    : integer;

              pfixsize    : byte
            end;

  constrec = record
               name : string;
               val  : int64
             end;

  varrec = record
             lineref : integer;

             name  : string;
             addr  : data;
             len   : data;

             arr : boolean;

             allocatedafter : boolean;

             init    : boolean;
             initlen : data;
             initpos : integer
           end;

var
  fin, fout : string;

  comms    : array [1..maxcommands] of commrec;
  commsnum : integer;

  cons     : array [1..maxconstants] of constrec;
  consnum  : integer;

  vars    : array [1..maxvars] of varrec;
  varsnum : integer;

  verbose : boolean;



  function xpos (pos:integer) : integer;
  var i:integer;
  begin
    i := pos-1;
    while (i>=0) and (chbuff[i]<>eol) do i:=i-1;
    xpos := pos-i
  end;

  function ypos (pos:integer) : integer;
  var y,i : integer;
  begin
    y := 1;
    for i := 0 to pos-1 do
      if chbuff[i]=eol
      then y := y+1;
    ypos := y
  end;

  function strline (pos:integer) : string;
  var
    s     : string;
    ls,le : integer;
    i     : integer;
  begin
    ls := pos - xpos(pos) + 1;
    le := ls;
    while not (chbuff[le] in [eol,eot]) do
      le := le+1;
    le := le-1;
    s := '';
    for i := ls to le do s := s + chbuff[i];
    strline := s
  end;

  function lineendpos (pos:integer) : integer;
  var le : integer;
  begin
    le := pos;
    while not (chbuff[le] in [eol,eot]) do
      le := le+1;
    lineendpos := le
  end;

  procedure addbyte (b:byte);
  begin
    if datapos >= buffersize
    then error ('memory overflow in compiler - increase buffersize');
    chbuff[datapos] := chr(b);
    datapos := datapos+1
  end;



  procedure readasm (fin, fout : string);
  const
    letter    : set of char = ['a'..'z','A'..'Z'];
    digit     : set of char = ['0'..'9'];
    hexdigit  : set of char = ['0'..'9','a'..'f','A'..'F'];
    idch      : set of char = ['0'..'9','a'..'z','A'..'Z','_'];
    numstart  : set of char = ['0'..'9','#','-',''''];
    datastart : set of char = ['0'..'9','#','-','''','"','['];

  var
    f : text;
    s : string;

    chpos, errpos : integer;
    currline : integer;

    ch  : char;
    w   : string;
    num : data;

    cont, first : boolean;
    aftersep    : boolean;

    pc   : integer;

    firstline : integer;



    procedure appends (s:string);
    var i : integer;
    begin
      if chbufflen+length(s) >= buffersize
      then error ('Source file to big, maximum size is '+strfromnum(buffersize)+' characters');
      for i := 1 to length(s) do
      begin
        chbuff[chbufflen] := s[i];
        chbufflen := chbufflen + 1
      end
    end;

    procedure markerr;
    begin
      errpos := chpos
    end;

    procedure err (s:string);
    begin
      if fileexists (fout)
      then deletefile (fout);
      writeln (strline(errpos));
      writeln ('^':xpos(errpos));
      write (fin,'(',ypos(errpos),',',xpos(errpos),') - ');
      error (s)
    end;

    function labelcomm (s:string) : integer;
    var i : integer;
    begin
      i := 1; while (i<commsnum) and not (comms[i].lab = s) do i := i + 1;
      if comms[i].lab = s
      then labelcomm := i
      else labelcomm := 0
    end;

    function nextch : char;
    begin
      nextch := chbuff[chpos+1]
    end;

    procedure getch;
    begin
      if ch<>eot
      then chpos := chpos + 1;

      ch := chbuff[chpos];

      if chpos = 0
      then currline := 1
      else if chbuff[chpos-1]=eol
           then currline := currline+1
    end;

    procedure escapedch;
    begin
      if ch=escch
      then begin
             getch;
             case ch of
               escch, '''','"' : skip;
               'n' :             ch := eol;
               't' :             ch := tab;
               '0' :             ch := chr(0);
               else begin markerr; errpos := errpos-1; err('invalid escape sequence') end
             end
           end
    end;

    function comment : boolean;
    begin
      comment := (ch=commentch) or ((ch=altcomment[1]) and (nextch=altcomment[2]))
    end;

    procedure skiptoeol;
    begin
      while not (ch in [eol,eot]) do
        getch
    end;

    procedure skipspacesinline;
    begin
      while comment or (ch in [' ',tab]) do
        if comment
        then skiptoeol
        else getch
    end;

    procedure skipspaces;
    begin
      while comment or (ch in [' ',tab,eol]) do
        if comment
        then skiptoeol
        else getch
    end;

    procedure skipch (e:char);
    begin
      if ch <> e
      then begin markerr; err (strfromch(e)+' expected') end;
      getch
    end;

    procedure expectch (ch:char);
    begin
      getch;
      skipch (ch);
      skipspacesinline
    end;

    procedure expecteol;
    begin
      markerr;
      if not (ch in [eol,eot])
      then err ('end of line or end of file expected');
      getch
    end;

    procedure getid;
    begin
      w := '';
      while ch in idch do
      begin w := w+ch; getch end;

      if (w='') or (w=eol)
      then err ('identifier expected');

      w := lowercase (w);
      skipspacesinline
    end;

    procedure skipw (s:string);
    begin
      if w<>s
      then err(''''+s+''' expected');
      skipspaces
    end;

    procedure expectw (s:string);
    begin
      markerr;
      getid;
      if w<>s
      then err(''''+s+''' expected');
      skipspaces
    end;

    procedure newcommand (line:integer);
    begin
      commsnum := commsnum + 1;
      if commsnum>maxcommands
      then err ('to many instructions, maximum is '+strfromnum(maxcommands)+' instructions');

      with comms[commsnum] do
      begin
        lineref  := line;
        bytepos  := pc;
        hasdata  := false;
        bytesnum := 0;
        lab      := '';
        oplabel  := ''
      end
    end;

    procedure addbyteincomm (b:byte);
    begin
      with comms[commsnum] do
      begin
        bytesnum := bytesnum + 1;
        if bytesnum = 1
        then bytes := datapos;
        addbyte (b)
      end
    end;

    function blockname (w:string) : boolean;
    var i : integer;
    begin
      i := 1; while (i<blockslen) and (blocks[i]<>w) do i := i+1;
      blockname := (blocks[i]=w)
    end;

    function constpos (w:string) : integer;
    var i : integer;
    begin
      i := 1; while (i<consnum) and (cons[i].name<>w) do i := i+1;
      if (consnum>0) and (cons[i].name = w)
      then constpos := i
      else constpos := -1
    end;

    function varpos (w:string) : integer;
    var i : integer;
    begin
      i := 1; while (i<varsnum) and (vars[i].name<>w) do i := i+1;
      if (varsnum>0) and (vars[i].name = w)
      then varpos := i
      else varpos := -1
    end;

    procedure getnumber;
    var
      h  : byte;
      i  : int64;
      j   : integer;
      neg : boolean;

      procedure gethex;
      begin
        case ch of
          'A'..'F' : h := ord(ch)-ord('A')+10;
          'a'..'f' : h := ord(ch)-ord('a')+10;
          '0'..'9' : h := ord(ch)-ord('0');
          else begin markerr; err ('hex digit expected') end
        end;
        getch
      end;

      procedure getdigit;
      begin
        if ch in digit
        then h := ord(ch)-ord('0')
        else begin markerr; err ('digit expected') end;
        getch
      end;

    begin

      num := 0;

      case ch of
        '#' : begin
                getch;

                i := 0;
                repeat
                  gethex;
                  num := (num << 4) + h;
                  i := i+1
                until (i=nibbles) or not (ch in hexdigit);

                if not (ch in [' ',tab,eol,eot,commentch,'-',';'])
                then begin markerr; err ('white space, comment, end of line or ; expected') end
              end;
        '-', '0'..'9' :
              begin
                neg := false;
                if ch = '-'
                then begin getch; neg := true end;

                i := 0;
                j := 0;
                markerr;
                repeat
                  getdigit;
                  i := i*10 + h;
                  j := j+1;
                until (j>maxdigitsinnum) or not (ch in digit);
                if (j>maxdigitsinnum) and (ch in digit)
                then begin
                       markerr;
                       err ('to many digits, maximum is '+strfromnum(maxdigitsinnum)+' digits')
                     end;

                if neg then i:=-i;

                if (i>maxnum) or (i<minsigned)
                then err ('value beetwen '+strfromnum(minsigned)+' and '+strfromnum(maxnum)+' expected')
                else num := i
              end;
        'A'..'Z','a'..'z' :
              begin
                markerr;
                getid;
                if constpos(w) = -1
                then err ('constant name or numeric constant expected')
                else num := cons[constpos(w)].val
              end;
        '''' : begin
                getch;
                escapedch;
                num := ord(ch);
                expectch('''')
              end
        else  begin markerr; err ('constant name or numeric constant expected') end

      end;

      skipspacesinline
    end;

    procedure processline;

      procedure addlabel;
      var
        l : integer;
      begin

        if length(w) > maxlabellen
        then err ('label to long, max length is '+strfromnum(maxlabellen)+' characters');
        if reserved (w)
        then err ('reserved word can not be used as label name');
        if constpos (w) <> -1
        then err ('constant name can not be used as label name');
        if varpos (w) <> -1
        then err ('variable name can not be used as label name');

        newcommand (currline);

        l := labelcomm (w);
        if l<>0
        then err ('duplicate label')
        else comms[commsnum].lab := w;

        skipch (':');
        skipspacesinline
      end;

      procedure getinst;
      var
        m : integer;
        d : integer;

      begin

        m := mnem (w);
        if m = -1
        then err ('instruction mnemonic expected');

        newcommand (currline);
        with comms[commsnum] do
        begin

          inst := m;

          bytepos  := pc;
          pfixsize := 0;

          if extendedinst (inst) {no operands}
          then if inst < ord (stop)
               then begin {opr}
                      oreg := inst - 16;
                      inst := ord (opr);
                      pfixsize := ord (oreg > 15)
                    end
               else begin {svc}
                      oreg := inst-ord(stop);
                      inst := ord(svc);
                    end
          else if ch in numstart
               then begin
                       getnumber;
                       oreg := num;
                       pfixsize := ord (oreg>15)
                     end
               else if not (ch in letter)
                    then err ('data value, constant, variable or instruction label expected')
                    else begin

                           markerr;
                           getid;
                           if reserved (w) and (w<>'end')
                           then err ('reserved word can not used as operand for instruction');

                           if coderefinst (inst)
                           then begin
                                  oplabel    := w;
                                  oplabelpos := errpos
                                end
                           else begin
                                  d := constpos (w);
                                  if d<>-1
                                  then oreg := cons[d].val
                                  else begin
                                         d := varpos (w);
                                         if d=-1
                                         then err ('contant or variable expected')
                                         else if ch<>'.'
                                              then if not vars [d].allocatedafter
                                                   then oreg := vars [d].addr
                                                   else begin
                                                          oplabel    := w;
                                                          oplabelref := d
                                                        end
                                              else begin
                                                     skipch ('.');
                                                     expectw ('len');
                                                     if vars [d].arr
                                                     then oreg := vars[d].len
                                                     else err('only array''s length can be accessed via .len')
                                                    end
                                       end
                                end;

                           {monkey patch}
                           if oplabel='end' then w := ''
                         end;

          pfixsize := ord (oreg>15);
          pc := pc + 1 + pfixsize
        end
      end;

    begin
      aftersep := false;
      cont     := true;

      while cont do
      begin
        if comment
        then skiptoeol
        else if ch in [eol,eot]
             then cont := false
             else begin

                    markerr;
                    aftersep := false;

                    if not (ch in letter)
                    then begin
                           cont     := false;
                           aftersep := true
                         end
                    else begin
                            getid;
                            if w = 'end'
                            then cont := false
                            else if ch = ':'
                                 then addlabel
                                 else begin
                                        getinst;
                                        if ch = ';'
                                        then begin aftersep := true; skipch(';'); markerr; skipspacesinline end
                                      end
                          end


                  end
      end;

      if aftersep
      then err ('label or instruction expected')
    end;

    procedure checklabelsandvars;
    var
      i : integer;

    begin
      for i := 1 to commsnum do
        with comms[i] do
          if (oplabel<>'') and coderefinst (inst)
          then begin
                 oplabelref := labelcomm (oplabel);
                 if oplabelref = 0
                 then begin errpos := oplabelpos; err ('undefined label - '+oplabel) end
               end
    end;

    procedure recalculatelabelsandvars;
    var
      changes : boolean;

      procedure calculatelabelsandvars;
      var
        i    : integer;
        diff : integer;

        procedure incpc (pos:integer; num:integer);
        var i : integer;
        begin
          for i := 1 to commsnum do
            with comms[i] do
              if bytepos>pos
              then bytepos := bytepos+num;
          for i := 1 to varsnum do
            with vars[i] do
              if allocatedafter
              then if addr>pos
                   then addr := addr+num
        end;

      begin
        for i := 1 to commsnum do
          with comms[i] do
            if oplabel<>''
            then begin
                   if not coderefinst (inst)
                   then with vars[oplabelref] do oreg := addr
                   else begin
                          diff := comms[oplabelref].bytepos-(bytepos+pfixsize+1);
                          if diff<0 then diff := 256+diff;
                          oreg := diff
                        end;
                   if (oreg > 15) and (pfixsize = 0)
                   then begin
                          changes := true;
                          incpc (bytepos,1);
                          pfixsize := 1
                        end
                 end
      end;

    begin
      repeat
        changes := false;
        calculatelabelsandvars;
      until not changes
    end;

    procedure addconstant (s:string; v:int64);
    begin
      consnum := consnum+1;
      cons[consnum].name := s;
      cons[consnum].val  := v
    end;

    procedure getconstantdef;
    var
      constindef : integer;
      i : integer;

      procedure checkconstantname;
      begin
        if reserved(w)
        then if first
             then err ('reserved word can not be used as constant name')
             else err ('constant name, const, var or begin expected');

        if constpos(w) <> -1
        then err ('constant '''+w+''' is already defined');
        if varpos (w) <> -1
        then err ('identifier '''+w+''' is already declared as variable');

        if consnum = maxconstants
        then err ('to manu constants, maximum is ' + strfromnum (maxconstants));
      end;

    begin
      if blockname(w)
      then if first
           then err ('reserved word can not be used as constant name')
           else skip
      else begin

             checkconstantname;
             constindef := 1;
             addconstant(w,0);

             while ch = ',' do
             begin
               getch;
               skipspaces;
               markerr; getid;
               checkconstantname;
               constindef := constindef+1;
               addconstant (w,0);
             end;

             skipch ('=');
             skipspaces;
             getnumber;

             for i := 1 to constindef do
               with cons[consnum-i+1] do
                 val := num;

             if not (ch in [';',eol,eot])
             then begin markerr; err ('; or end of line expected') end;

             first := ch = ';';
             getch;
             if first
             then skipspacesinline
             else skipspaces;

             markerr; getid; skipspaces;
             cont := not blockname(w) or first
           end
    end;

    procedure getvardef;
    var
      varsindef : integer;

      ip : integer;
      il : integer;

      i : integer;

      procedure addvariable (s:string);
      begin
        varsnum := varsnum+1;
        with vars[varsnum] do
        begin
          lineref := ypos(errpos);
          name := s;

          len  := 1;
          arr  := false;
          init := false;

          if ch<>'>'
          then allocatedafter := false
          else begin
                 getch; skipspacesinline;
                 allocatedafter := true
               end;

        end
      end;

      procedure checkvariablename;
      begin
        if reserved(w)
        then if first
             then err ('reserved word can not be used as variable name')
             else err ('variable name, const, var or begin expected');

        if varpos(w) <> -1
        then err ('variable '''+w+''' is already defined');
        if constpos (w) <> -1
        then err ('identifier '''+w+''' is already declared as const');

        if consnum = maxconstants
        then err ('to manu constants, maximum is ' + strfromnum (maxconstants))
      end;


      procedure addnumber (num:data);
      begin
        if il=0
        then ip := datapos;
        il := il+1;

        if datapos >= buffersize
        then error ('memory overflow - increase buffersize');
        chbuff[datapos] := chr(num);
        datapos := datapos+1;
      end;

      procedure addnumbers;
      var i, rep : integer;
      begin
        getnumber; rep := 1;
        if not (ch in [',',']'])
        then begin
               rep := num;
               expectw ('times');
               getnumber
             end;
        for i := 1 to rep do
          addnumber (num)
      end;

    begin
      if blockname(w)
      then if first
           then err ('reserved word can not be used as variable name')
           else skip
      else begin

             varsindef := 1;
             checkvariablename;
             addvariable (w);

             while ch = ',' do
             begin
               getch;
               skipspaces;
               markerr; getid;
               checkvariablename;
               varsindef := varsindef+1;
               addvariable (w);
             end;

             if ch = 'i'
             then begin

                    il := 0;
                    expectw ('init');
                    {skipch ('=');
                    skipspaces;}

                    case ch of
                      '[' : begin
                              vars[varsnum].arr := true;
                              skipch('[');
                              addnumbers;
                              while ch = ',' do
                              begin
                                skipch (',');
                                skipspaces;
                                addnumbers
                              end;
                              skipch (']')
                            end;
                      '"' : begin
                              vars[varsnum].arr := true;
                              skipch('"');
                              while not (ch in [eol,eot,'"']) do
                              begin
                                escapedch;
                                addnumber (ord(ch));
                                getch
                              end;
                              skipch ('"');
                              if il=0
                              then begin markerr; errpos := errpos-1; err ('String must contain at least one character') end
                            end;
                      else begin
                             getnumber;
                             addnumber (num)
                           end
                    end;
                    for i := 1 to varsindef do
                      with vars[varsnum-i+1] do
                      begin
                        init    := true;
                        initlen := il;
                        initpos := ip;
                        len := initlen
                      end
                  end;

             if not (ch in [';',eol,eot])
             then begin markerr; err ('; or end of line expected') end;

             first := ch = ';';
             getch;
             if first
             then skipspacesinline
             else skipspaces;

             markerr; getid;
             cont := not blockname(w) or first
           end
    end;

    procedure allocatevars (after:boolean; lref:integer);
    var
      len : data;
      i   : integer;

      procedure addinst (m:mnemonic; op:data);
      begin
        newcommand (firstline);
        with comms[commsnum] do
        begin
          inst := ord(m);
          oreg := op;
          pfixsize  := ord (oreg>15);
          bytepos := pc;
          pc := pc+1+pfixsize;
        end
      end;

    begin

      if not after
      then begin
             len := 0;
             for i := 1 to varsnum do
               if not vars[i].allocatedafter
               then len := len+vars[i].len;
             if len<>0 then addinst (br,len)
           end
      else for i := 1 to varsnum do
             with vars[i] do
               if allocatedafter
               then lineref := lref;

      for i := 1 to varsnum do
        with vars[i] do
          if allocatedafter = after
          then begin
                 addr := pc;
                 newcommand (lineref);
                 comms[commsnum].hasdata := true;
                 if not init
                 then addbyteincomm (0)
                 else with comms[commsnum] do
                      begin
                        bytes    := initpos;
                        bytesnum := initlen
                      end;
                 pc := pc+len
               end
    end;


  begin

    {read file into buffer}
    assign (f,fin);
    reset (f);

    chbufflen := 0;
    while not eof(f) do
    begin
      s := '';
      while not eoln(f) and not eof(f) do
      begin
        read(f,ch);
        s := s+ch
      end;
      appends(s);
      if eof (f)
      then appends (eot)
      else begin
             readln  (f);
             appends (eol);
           end
    end;
    close (f);

    {chbuffer can be used for temporray allocations during compilation}
    datapos := chbufflen;

    {init}
    commsnum := 0;

    consnum := 0;
    varsnum := 0;

    pc := 0;

    {init ch}
    chpos := -1; getch;

    skipspaces;
    firstline := currline;
    markerr; getid; skipspaces;
    if (w<>'hex') and (w<>'hex8')
    then err ('hex8 or hex expected');

    markerr; getid; skipspaces;
    if reserved (w)
    then err ('program name expected');
    addconstant ('bpw',1);

    markerr; getid; skipspaces;
    if not blockname (w)
    then err ('const, var or begin expected');

    while w<>'begin' do
    begin
      if w = 'const'
      then begin
             markerr; getid; skipspaces;
             first := true;
             repeat
               getconstantdef
             until not cont
           end;
      if w = 'var'
      then begin
             markerr; getid; {skip only to end of line?}
             first := true;
             repeat
               getvardef
             until not cont
           end
    end;

    {after allocation, code start addr is known}
    allocatevars (false, 0);

    skipw ('begin');
    {newcommand (ypos(errpos));
    comms[commsnum].lab := 'start';}

    while (w<>'end') and (ch<>eot) do
    begin
      if ch = eot
      then begin markerr; err ('label, instruction or end expected') end;

      processline;

      if (w<>'end') and (ch<>eot)
      then begin
             skipch (eol);
             skipspacesinline {only to end of line?}
           end
    end;

    skipw ('end');
    skipch ('.');

    {add end label}
    newcommand (currline);
    comms[commsnum].lab := 'end';

    {add stop}
    newcommand (currline);
    with comms[commsnum] do
    begin
      inst := ord(svc);
      oreg := 0;
      pfixsize := ord (oreg>15);
      bytepos := pc;
      pc := pc+1;
    end;

    {add variables after}
    allocatevars (true, currline);

    {generate real prefixes for labels and variables allocated after}
    checklabelsandvars;
    recalculatelabelsandvars
  end;



  procedure writehex (fn:string);
  const
    perline = 25;

  var
    f : text;
    i : integer;

    total : integer;

    firstbyte : integer;
    nrofbytes : integer;

    isinst  : boolean;

    currl   : integer;
    currpos : integer;
    currcom : integer;

    pc : integer;

    procedure outbyte (b:byte);
    begin
      write (f,zext(hexstr(b),2),' ')
    end;

    procedure outb (i:integer);
    begin
      if i<=nrofbytes
      then outbyte (ord(chbuff[firstbyte+i-1]))
      else if verbose then write (f, ' ':3)
    end;

    procedure outbyteplain (b:byte);
    begin
      total := total+1;
      outbyte (b);
      if (total mod perline) = 0
      then writeln (f)
    end;

    procedure addbyteinline (b:byte);
    begin
      nrofbytes := nrofbytes + 1;
      addbyte (b);
    end;

    procedure geninst (pfixsize:byte; inst:mnemonic; oreg:data);

      procedure gen (m:mnemonic; d:data);
      var b : byte;
      begin
        b := (ord(m)<<4) or (d and $0f);
        addbyteinline (b)
      end;

    begin
      if (pfixsize<>0) or (oreg>15)
      then gen (pfix, oreg>>4);
      gen (inst, oreg)
    end;

  begin
    assign (f,fn);
    rewrite (f);

    total := 0;

    currl    := 1;
    currpos  := 1;
    currcom  := 1;

    while currpos < chbufflen do
    begin

      firstbyte := datapos;
      nrofbytes := 0;
      isinst    := true;

      pc := -1;
      while comms[currcom].lineref = currl do
      begin
        with comms[currcom] do
        begin
          if pc=-1 then pc := bytepos;
          if lab<>'' {is label}
          then isinst := false
          else if bytesnum=0 {is instruction else is data}
               then geninst (pfixsize, mnemonic(inst), oreg)
               else begin isinst := false; for i := 1 to bytesnum do addbyteinline (ord(chbuff[bytes+i-1])) end
        end;
        currcom := currcom + 1
      end;

      if not verbose
      then for i := 1 to nrofbytes do outbyteplain (ord(chbuff[firstbyte+i-1]))
      else for i := 0 to (nrofbytes-1) div 2 do
           begin
             outb (i*2+1);
             outb (i*2+2);
             if (i=0) and not isinst
             then write (f,commentch,zext(strfromnum(pc),3))
             else write (f,' ':4);
             write (f,commentch);
             if i<>0
             then writeln (f)
             else writeln (f,' ',strline(currpos))
           end;

      datapos := firstbyte;
      currpos := lineendpos(currpos)+1;
      currl   := currl+1
    end;

    close(f)
  end;

begin

  verbose := false;
  case paramcount of
    0 : error ('name of '+asmext+' file expected');
    1 : skip;
    2 : if paramstr(2) = '-v'
        then verbose := true
        else error ('second parameter can be only "-v" to enable verbose output');
    else error ('command syntax is: '+asmext+' fname [-v]')
  end;
  fin := paramstr(1);
  if fin[length(fin)]<>'.' then fin := fin+'.';
  fout := fin+hexext;
  fin := fin+asmext;

  if not fileexists (fin) then error ('File '+fin+' don''t exists');

  readasm  (fin, fout);
  writehex (fout)

end.
