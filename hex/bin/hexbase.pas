unit hexbase;

interface

uses
  base;

const
  asmext = 'asm8';
  hexext = 'hex8';

type    data = byte;      { 8 bits - byte; 16 bits - word; 32 bits - cardinal; 64 bits - qword }
const   bytesperword = 1; { 1, 2, 4 or 8 }

const
  nibbles = 2*bytesperword;

var
  maxnum    : word64;
  maxsigned : int64;
  minsigned : int64;

type
  mnemonic = (ldam, ldbm, stam, ldac, ldbc, ldap, ldai, ldbi,
              stai, br,   brz,  brn,  svc,  opr,  pfix, nfix,
              brb,  add,  sub,
              stop, get,  put,  putn);

const
  mnems : array [mnemonic] of string[4] =
               ('ldam', 'ldbm', 'stam', 'ldac', 'ldbc',  'ldap', 'ldai',  'ldbi',
                'stai', 'br',   'brz',  'brn',  'svc',   'opr',  'pfix',  'nfix',
                'brb',  'add',  'sub',
                'stop', 'get',  'put', 'putn');

  msnum  = ord(high(mnemonic));
  oprmax = ord(stop)-17;
  svcmax = msnum-ord(stop);

  rsrvdlen = 9;
  rsrvd : array [1..rsrvdlen] of string [5] =
            ('areg', 'breg', 'oreg', 'pc', 'hex', 'hex8', 'times', 'init', 'end');

  blockslen = 3;
  blocks : array [1..blockslen] of string[5] =
             ('const', 'var', 'begin');

  commentch  = '|';
  altcomment = '--';
  escch      = '\';

const
  memsize = 256;

var
  mem : array [0..memsize-1] of byte;

  areg : byte;
  breg : byte;
  oreg : byte;
  pc   : byte;

  inst    : byte;
  running : boolean;

  function mnem (w:string) : integer;
  function extmnem : mnemonic;

  function extendedinst (inst:integer) : boolean;
  function coderefinst  (inst:integer) : boolean;
  function constinst    (inst:integer) : boolean;

  function reserved (w:string) : boolean;


implementation

  function mnem (w:string) : integer;
  var i : integer;
  begin
    i := 0;
    while (i<msnum) and not (mnems[mnemonic(i)]=w) do i := i + 1;
    if mnems[mnemonic(i)]=w then mnem := i else mnem := -1
  end;

  function extmnem : mnemonic;
  begin
    case mnemonic (inst) of
      opr : if oreg<=oprmax
            then extmnem := mnemonic (16+oreg)
            else extmnem := stop;
      svc : if oreg<=svcmax
            then extmnem := mnemonic (ord(stop)+oreg)
            else extmnem := stop;
      else extmnem := mnemonic(inst)
    end
  end;

  function extendedinst (inst:integer) : boolean;
  begin
    extendedinst := inst >= 16
  end;

  function coderefinst (inst:integer) : boolean;
  begin
    coderefinst := mnemonic(inst) in [ldap, br, brz, brn]
  end;

  function constinst (inst:integer) : boolean;
  begin
    constinst := mnemonic(inst) in [ldac, ldbc]
  end;

  function reserved (w:string) : boolean;
  var i : integer;
  begin
    if mnem(w) <> -1
    then reserved := true
    else begin
           i := 1;
           while (i<rsrvdlen) and not (rsrvd[i]=w) do i := i + 1;
           if rsrvd[i] = w
           then reserved := true
           else begin
                  i := 1;
                  while (i<blockslen) and not (blocks[i]=w) do i := i + 1;
                  reserved := blocks[i] = w
                end
         end
  end;


var
  i : integer;

begin

  maxnum := 0;
  for i := 1 to nibbles do
    maxnum := (maxnum << 4) or $f;

  maxsigned := maxnum div 2;
  minsigned := -(maxsigned+1)

end.
